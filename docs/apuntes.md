# Apuntes

## Reponsables

- John Fredy Marulanda Correa (<jmarulanda@grupo-exito.com>)
- Guiovanni Gualteros (<ggualteros@grupo-exito.com>)

## Apoyo técnico

- Jose Bernardo Arcos
- Duván Agudelo

## Contexto

Ventas de Contenido es un canal por el cual se venden servicios de terceros tales como ___telefonia___, ___streaming___ y ___membresias___ entre otras. El proceso consiste en indicarle al POS el PLU asociado al servicio (representado en tarjetas):

<figure markdown="span">
    ![tarjetas](assets/notas/001.png)
    <figcaption>Tarjetas de Membresía</figcaption>
</figure>

<figure markdown="span">
    ![tarjetas](assets/notas/002.png)
    <figcaption>Código de barras de la tarjeta</figcaption>
</figure>

Seguidamente se escanea el código de barras de la tarjeta (1). En la mayoria de casos, las tarjetas cuentan con un valor asignado de recarga (2) con excepcion de algunas tarjetas de telefonia que si permiten agregar un valor de recarga indefinido.

El POS envia la informacion al autorizador y este puede responder:

1. ___Aceptando la intención de activación___: En caso de aceptación, el servicio no queda recargado hasta que no se confirma el pago de la tarjeta.<sup>(revisar)</sup> Este pago se puede realizar por cualquier tipo de medio dispuesto para ello.

2. ___Rechazando la intencion de activación___: En caso de rechazo, el POS muestra un mensaje genérico de rechazo.<sup>(revisar)

## Componentes

- ___Archivo JAR___: [`VentaContenidos.jar`](<https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-contenido-termFw-VentaContenidos>)

- ___Archivo HCH___: [`HCHCONTE.JAR`](<https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-ventaContenido-contPAFHCH-hchconte>)

- ___Dependencia___: [`FrameworkContenidosPagos.jar`](<https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-reglas-termFw-FrameworkContenidosPagos>)

- ___Archivo FW___: `FWVDCONT.DAT`

- ___Archivo BAS___: `uefrwmod`, `uefrwmo2`, `uefrmo3`

## Preguntas

- [x] ___Diferencia entre FW y Dominio___
  - Implementacion de soluciones que escalan la funcionalidad del POS
  - [x] ___Dominio___: Es escalable y permite la integreacion de varias soluciones
    - Persistencia manejada con BBDD
    - Comunicacion de comunicaciones propias (Servidor de Aplicaciones)
  - [x] ___Framework___: Enfocada en un tipo de soluciones especificas... no permitia la escalabilidad por su complejidad.
    - Venta: Es la venta de un PLU. Lon ingreso proviene del ejercicio del negocio.
    - Pago: Es el movimiento de dinero en ambas direcciones que pueden tener o no relacionados un PLU. Los ingresos se dan por el movimiento de dinero.
    - Operación No Venta: Operaciones que no generan ingresos.
    - Persistencia acoplado a ___TOSHIBA___ (Archivos KEYED)
    - Comunicación mediante PAF
- [x] ___¿Cual es la filosofia en la migración... que se pretende mejorar?___
  - Escalabilidad
  - Persistencia
  - Comunicación
  - Mantenibilidad
- [x] ___¿Han habido intentos anteriores de migración y si ha sido asi en que han fallado?___
  - Ya se han migrado servicios de manera completa
  - Otras aun existen en Framework por falta de tiempo/recursos
  - _Tarjetas Regalo_ y _Convenios_ existen en ambas arquitecturas (hibrido)
- [ ] ___¿Hay lineamientos en la migracion actualmente?___
  - Que tipo de producto se desea migrar (reglas de negocio)
- [ ] ___¿Homologos de componentes entre arquitecturas (Ej HCH's)?___

- [ ] ___¿Hay módulo de activación? - ¿Se permite reversar la compra despues del pago?___
    <pendiente>
- [x] ___¿Cuales son los servicios que se venden en los puestos de pago?___
  - Netflix
  - PSStore
  - Xbox
  - Recarga a operadores de telefonia (distintos a Movil Exito)
- [ ] ___¿Los atributos de configuracion se deben externalizar? (por fuera de la compilación)___
  <pendiente>
- [x] ___¿Qué componente de BASIC empuja la ejecución de ventaContenido?___
  - `uefrwmod`
  - `uefrwmo2`
  - `uefrmo3`
- [ ] ___¿Qué es Electronic Journal?___
  <pendiente>
- [ ] ___¿Qué es TLog?___
  <pendiente>
- [x] ___¿Qué es Middleware?___
  Es el componente que se encarga de redireccionar las peticiones hacia framework o Dominio
- [ ] ___¿Qué es Middleware Cloud?___
- [x] ___¿Qué es Interface?___
  Es el encargado de interpretar el mensaje recibido via `Middleware` desde `BASIC` y enviarlo al `Director`
- [x] ___¿Qué es DataEntry?___
  Es un conjunto de subgrupos que se encargan de _marcar_ un item con unas caracteristicas que desencadenan flujos especiales al momento de la venta y pago. Por ejemplo la venta de una tarjeta de Netflix.
- [x] ___¿Qué son CallJavaBasic?___
  Es el protocolo de comunicación usado actualmente para transferir información entre BASIC y JAVA.
- [x] ___¿Qué diferencia Pago, Venta y No Venta?___
  ___Venta___ es la intención de compra de un item por parte de un cliente y que afecta tantos los inventarios como el flujo de dinero de ls compañia
  ___No Venta___ es la operación que no afecta los inventarios de la compañia y presta el servicio de un tercero como son las recargas, lo giros, las apuestas, etc.
  ___Pago___ es el último paso que cierra la transacción y asienta la venta de un item.
- [ ] ___¿La comunicacion va a ser ISO o HTTP?___
  <pendiente>

## Tareas

- [x] revisar la arquitectura actual (framework)
  - [ ] Entender el flujo del proceso y la relación de componentes
- [x] revisar la arquitectura a implementar (dominio)
  - [ ] consultar migraciones previas
- [ ] Solicitar ambiente
- [ ] Si es relevante, conocer los PLU de los servicios ofrecidos
- [x] Revisar componentes de producción

## Mis Notas

- ISOService: componente del lado del Dominio para manejar las tramas ISO8583
- JSON con capa de seguridad
- Operaciones
  - Pagos
  - Ventas
  - Intercambio de medio de pago
  - Eventos
- Objeto POSTransaction: Estandar de todos los productos del negocio. Es decir, que atributos hacen parte de cada transaccion dependiendo el producto.
- Orden jeraquico:
  - `VentaContenidos.jar` (bp)
  - `FrameworkContenidosPagos.jar` (platform)
  - `Reportes.jar`, `Persist.jar` o `Presentacion.jar` (libs)
- Arquitectura
  - Framework
    - <figure markdown="span">
        ![ark](assets/notas/ModelodeFramework.png)
        <figcaption>Arquitectura Framework</figcaption>
      </figure>

  - Dominio
    - <figure markdown="span">
        ![ark](assets/notas/ModelodeDominio1.png)
        <figcaption>Arquitectura Dominio</figcaption>
      </figure>

- Modelo de Dominio
  - immergeTransportObject
  - emmergeTransportObject
  - transaccion positiva: Se ingresa al modelo de dominio
  - transaccion negativa: Se retira al modelo de dominio
  - bifrost: afectación de la base de datos
  - prevalidaciones
  - posvalidaciones
  - Dia de Duvan y Bernie
  - Tener un Diseño con base a recomendaciones
  - ___Continuidad___: Autorizador falla no se bloquee el POS (Resiliencia)
  - ___Trazabilidad___: Logs a escribir

## Componentes relacionados

- `fwvdc.dat`: Productos para la venta
- `fwvdcont.dat`: Configuraciones del Product Bussiness

## RoadMap

### Etapa preliminar

<strong><sub>E: Estado actual - A: Acciones a seguir</sub></strong>

- [x] ___Evaluar el estado actual del software y sus dependencias:___
  - [x] Analizar el código fuente del software existente. (¿hay repos y estan actualizados?)
    - E: Hasta el momento no se encuentra discrepancia entre lo revisado y lo que está funcionando en producción
    - A: Seguir atento
  - [x] Revisar la documentación y comprender las dependencias del sistema.
    - E: La documentación da una vision general de las arquitecturas pero no está actualizada con los cambios posteriores.
    - A: Revisar los flujos
  - [x] Identificar posibles problemas o limitaciones del SW que no permitan la migración.
    - E: No se evidencian acoplamientos que dificulten la migración en este PB.
    - A: Revisar flujos
- [x] ___Analizar los riesgos y beneficios de la migración:___
  - [x] Identificar los posibles riesgos asociados con la migración.
    - Dependencias ocultas o Casos de Uso que no esten identificados en la primera inspección y dificulten la integracion en el DOM
  - [x] Evaluar los beneficios esperados al llevar a cabo la migración.
    - Escalabilidad
    - Mantenibilidad
    - Mejoras en Persistencia
    - Mejoras en Comunicación
- [ ] ___Definir el alcance de la migración:___
  - [x] Identificar los componentes específicos del sistema que se migrarán.
    - Se migrará la funcionalidad de __Ventas Contenido__ sin ninguna caracteristica nueva
  - [ ] Establecer los criterios tecnicos para determinar qué aspectos del sistema se mantendrán sin cambios. (MUY IMPORTANTE)
  - [ ] Considerar aspectos tecnicos que impacten el rendimiento y la escalabilidad.
- [ ] ___Establecer plazos de tiempo___
- [x] ___Realizar un inventario de componentes___
- [ ] ___Evaluar tecnologías a implementar:___
  - [ ] Detectar ventajas y desventajas de cada tecnología para tomar decisiones informadas. (MUY IMPORTANTE)
- [ ] ___Entender la arquitectura del DOM:___
  - [ ] Diseñar diagramas que representen la estructura y la interacción de los nuevos componentes del sistema. (RAYAR)
    - ![arq](assets/notas/prop-arq.png)
- [ ] ___Definir patrones de migración:___
  - [ ] Identificar los patrones de diseño adecuados para cada componente.
  - [ ] Determinar si se requiere refactorización, reingeniería u otros enfoques para migrar cada componente.
- [ ] ___Definir protocolos de comunicación a otra capas. (Pesistencia - Middleware Cloud)___
- [ ] ___Preparar el entorno de desarrollo y pruebas:___
  - [x] Configurar un entorno de desarrollo.
  - [ ] Establecer infraestructuras de prueba en entorno controlado. (MUY IMPORTANTE)

### Etapa de migración

- [ ] ___Ejecutar la migración del servicio:___
  - [ ] Realizar las acciones necesarias para migrar cada componente. (Creación de nuevos componentes del lado del controlador)
  - [ ] Transferir configuraciones y datos relevantes al nuevo entorno.
- [ ] ___Implementación de estrategia de registros (LOGS)___
- [ ] ___Realizar pruebas unitarias (integración - aceptación):___
  - [ ] Desarrollar casos de prueba para verificar el funcionamiento correcto de cada componente migrado. (QA)
  - [ ] Ejecutar pruebas unitarias para identificar y corregir posibles problemas. (IDEAL)
- [ ] ___Validación de la integridad de los datos migrados/generados:___
  - [ ] Verificar que los datos migrados sean consistentes y precisos.
  - [ ] Realizar pruebas integración. (QA)
- [ ] ___Observar el rendimiento:___
  - [ ] Monitorear el rendimiento del servicio en términos de eficiencia y resiliensia.
  - [ ] Ajustes (Refactory).
- [ ] ___Observar la escalabilidad:___
  - [ ] Enfoque a principio Open-Close.
  - [ ] Realizar pruebas de carga y estrés para medir la capacidad de escalabilidad del sistema. (IDEAL)

### Etapa posterior a la migración

- [ ] ___Documentación:___
  - [ ] Registrar todos los cambios realizados durante el proceso de migración ademas de actualizaciones de código, configuraciones y otros cambios relevantes.
- [ ] ___Realizar pruebas finales (Aseg. Calidad) y monitorear el funcionamiento del sistema (Piloto) (QA)___
- [ ] ___Evaluar el éxito de la migración en relación con los objetivos definidos___
- [ ] ___Ajustes y refactorización:___
  - [ ] Realizar ajustes adicionales o refactorización del sistema migrado en base a la retroalimentación y los resultados obtenidos.
  - [ ] Corregir posibles errores o deficiencias identificadas durante la etapa de pruebas.

## Diagrama de secuencia general

```mermaid
sequenceDiagram
    autonumber
    actor c as Cajero/POS
    participant basic as Basic
    participant gateway as TerminalGateway
    participant device as DeviceHandler
    participant collect as DataCollector
    participant params as ParametersStore
    participant domain as DomainEngine<br>(venta contenido)
    participant autor as Prov. Externo<br>(Autorizador)
    c->>basic: inicia petición
    Note right of c: registrar PLU<br>imprimir dato<br>finalizar trx<br>almacenar data
    basic->>gateway: activa el proceso y envía mensaje
    Note right of basic: datos de la operación<br>(formato json) 
    gateway->>collect: normaliza y redirige mensaje
    collect->>params: solicita configuración
        Note over params: los datos deben estar disponible offline
    params-->>collect: envía respuesta
    alt Necesita datos complementarios 
        collect->>device: solicita datos
        loop Tantas veces como datos sean pedidos
            device->>c: solicita datos
        Note over c, device: datos complementarios (según la operación)
            c-->>device: devuelve datos
        end
        device-->>collect: devuelve datos
    end
    collect->>collect: empaqueta el mensaje
    collect->>gateway:transmite mensaje
    gateway->>domain: redirige el mensaje
    domain->>domain: Aplica lógica
    domain->>autor: hace la petición
    alt Respuesta OK
        autor-->>domain: notifica
        domain-->>gateway: notifica
        gateway-->>basic: notifica
        basic-->>c: notifica
        c-xc: continua operación
    else Respuesta NOK
        autor-->>domain: notifica
        domain->>domain: interpreta y prepara respuesta
        domain-->>gateway: notifica
        gateway-->>basic: notifica
        basic-->>c: notifica
        c-xc: continua operación
    end
```

<figure markdown="span">
    ![tarjetas](assets/new-architecture/diagram01.svg)
    <figcaption>C4</figcaption>
</figure>

## Componentes

### TerminalGateway

|Definiciones||
|-|-|
|__Entorno:__|Terminal|
|__Vocación:__|Enfocado en la comunicación y enrutamiento de mensajes (orquestación del flujo)|
|__Interacciones:__|Recibe peticiones desde un cliente, orquesta hacia el resto del flujo, responde a la petición inicial|
|__DataInput:__|Objeto JSON/GRPC|
|__DataOutput:__|Objecto JSON/GRPC|

### DataCollector

|Definiciones||
|-|-|
|__Entorno:__|Terminal|
|__Vocación:__|Enfocado en la gestión y manipulación de datos en la terminal|
|__Interacciones:__|Solicita datos al DeviceHandler, Envía datos normalizados al TerminalGateway, Actualiza parámetros en LocalParameterStore|
|__DataInput:__||
|__DataOutput:__||

### DeviceHandler

|Definiciones||
|-|-|
|__Entorno:__|Terminal|
|__Vocación:__|Enfocado en la interacción con el usuario a través de dispositivos como el teclado y la pantalla|
|__Interacciones:__|Solicita y muestra información al usuario via dispositivos I/O, Valida los datos ingresados según el LocalParameterStore|
|__DataInput:__||
|__DataOutput:__||

### ParameterStore

|Definiciones||
|-|-|
|__Entorno:__|Multientorno|
|__Vocación:__|Proveer las configuraciones de negocio a los módulos solicitantes mediante peticiones.|
|__Interacciones:__|Comunicación con módulos cliente.|
|__DataInput:__||
|__DataOutput:__||

### LocalParameterStore

|Definiciones||
|-|-|
|__Entorno:__|Terminal|
|__Vocación:__|Proveer las configuraciones de negocio a los módulos de al terminal solicitantes mediante peticiones.|
|__Interacciones:__|Provee la cantidad y características de parámetros al DataCollector garantizando la alta disponibilidad|
|__DataInput:__||
|__DataOutput:__||

### DomainEngine

|Definiciones||
|-|-|
|__Entorno:__|Multientorno|
|__Vocación:__|Proveer la lógica necesaria para manejar, transformar, calcular y enrutar los datos recibidos con el fin de entregar un resultado que satisfaga las necesidades de la operación.|
|__Interacciones:__|Comunicación con módulos cliente.|
|__DataInput:__||
|__DataOutput:__||

### JBAdapter

|Definiciones||
|-|-|
|__Entorno:__|Terminal|
|__Vocación:__|Mediador entre JavaAPI y TerminalConfiguration|
|__Interacciones:__|JavaAPI - TerminalGateway|
|__DataInput:__||
|__DataOutput:__||

### AEFAdapter

|Definiciones||
|-|-|
|__Entorno:__|Terminal|
|__Vocación:__|Mediador entre AEF u otro mecanismo de persistencia del lado del POS y el DataCollector.|
|__Interacciones:__|AEF - DataCollector|
|__DataInput:__||
|__DataOutput:__||

### Tecnologías

- BASIC
- Java 1.8
- Spring Boot 2.7.12
- Maven
- junit 5.x
- Mockito 3.x
- JaCoCo
- Lombok
- Logback (definido por equipo a cargo)
- tomcat / tomEE
- rest / grpc
- swagger
- caffeine (manejo de caché)

### Patrones

- SAGAs (acción compensatoria) coreografía u orquestación
- (transaccionalidad distribuida)

### Arquitectura de puertos y adaptadores

Es un estilo de arquitectura de software que enfatiza la separación de la lógica de negocio de las dependencias técnicas externas. La idea es pensar en la aplicación como el artefacto central de un sistema, donde toda la entrada y salida llega o sale de la aplicación a través de un puerto que aísla la aplicación de herramientas, tecnologías y mecanismos de entrega externos. La aplicación no debería tener conocimiento de quién o qué está enviando la entrada o recibiendo su salida. Esto tiene como objetivo brindar cierta protección contra la evolución de la tecnología y los requisitos comerciales, que pueden hacer que los productos queden obsoletos poco después de su desarrollo, debido al bloqueo de la tecnología o del proveedor.

<figure markdown="span">
    ![tarjetas](assets/new-architecture/hexagonal.svg)
    <figcaption>Diagrama de puertos y adaptadores</figcaption>
</figure>

#### Capa de dominio

Contiene los elementos que representan el núcleo de la lógica de negocio:

- ___Entidades___: Son objetos del modelo de dominio que tienen identidad propia y un ciclo de vida persistente. Representan conceptos del negocio y contienen tanto datos como comportamiento.
- ___Agregados___: Son agrupaciones de entidades relacionadas que se tratan como una unidad única para ciertas operaciones de negocio. Cada agregado tiene una entidad raíz que garantiza la consistencia del agregado.
- ___Objetos de Valor___: Son objetos que se definen por sus atributos en lugar de una identidad propia. Representan conceptos inmutables y pequeños, como cantidades, fechas o coordenadas.
- ___Servicios de Dominio___: Contienen lógica de negocio que no encaja naturalmente dentro de una entidad o un objeto de valor. Son operaciones del dominio que involucran a múltiples entidades o agregados.
- ___Repositorios (Interfaz)___: Definen contratos para la persistencia y recuperación de agregados y entidades. Los repositorios en la capa de dominio son solo interfaces; las implementaciones están en los adaptadores.
- ___Fábricas___: Son responsables de la creación compleja de objetos de dominio, especialmente cuando la creación implica múltiples pasos o dependencias entre objetos.

#### Capa de aplicación

Actúa como intermediaria entre la capa de dominio y capa de infraestructura. Esta capa orquesta la lógica de negocio contenida en la capa de dominio, coordinando tareas y aplicando políticas.

- ___Casos de Uso___: Definen las operaciones específicas de la aplicación que un usuario puede realizar. Son métodos de alto nivel que utilizan los servicios de aplicación para ejecutar la lógica de negocio.
- ___Servicios de Aplicación___: Coordinan las operaciones del negocio, llamando a los servicios de dominio y manejando transacciones. No contienen lógica de negocio en sí misma, sino que orquestan las llamadas a los componentes de dominio.
- ___DTOs___: Transfieren datos entre las capas. Facilitan la serialización y deserialización de datos y son objetos simples sin lógica de negocio.

#### Capa de infraestructura

Contiene todos los detalles técnicos y las implementaciones concretas necesarias para que la aplicación funcione correctamente. Esta capa se encarga de proporcionar la infraestructura necesaria para soportar las operaciones definidas en las capas de aplicación y dominio.

- ___Implementaciones de Repositorios___: Implementan las interfaces de repositorio definidas en la capa de dominio, utilizando tecnologías específicas.
- ___Servicios de Infraestructura___: Proveen integración con sistemas externos, manejo de archivos, etc.
- ___Adaptadores de Entrada y Salida___: Implementan la lógica para comunicarse con sistemas externos (adaptadores de salida) y para recibir comunicaciones de sistemas externos o interfaces de usuario (adaptadores de entrada).
- ___Configuración___: Incluye archivos de configuración, configuración de dependencias, configuración de beans de Spring, etc.
- ___Módulos de Seguridad___: Manejan la autenticación y autorización de usuarios y servicios.

### Scaffolding basado en arquitectura de puertos y adaptadores

#### Quién eres y de qué tipo (Vertical Slicing - Screaming Arch)

___Vertical Slicing___ es una técnica de desarrollo que divide una aplicación en incrementos funcionales completos, cada uno abarcando todas las capas necesarias (UI, lógica de negocio y persistencia) para entregar valor de manera incremental y reducir riesgos. ___Screaming Architecture___, sugiere que la estructura del software debe reflejar claramente su propósito y dominio, con módulos organizados según los conceptos del negocio en lugar de detalles técnicos, lo que mejora la comprensión, mantenibilidad y flexibilidad del sistema.

#### Uso de Arquetipos

Un arquetipo asegura que todos los proyectos sigan una estructura coherente, elimina la configuración manual inicial, simplifica el arranque con un solo comando, y permite personalización y extensión según las necesidades específicas del proyecto.

- Estándares Consistentes.
- Ahorro de Tiempo.
- Facilidad de Uso.
- Flexibilidad.
  
<figure markdown="span">
    ![tarjetas](assets/new-architecture/scaffolding.svg)
    <figcaption>Arq. de puertos y adaptadores</figcaption>
</figure>

```plaintext
|   .gitignore
|   pom.xml
|   README.md
|
+---src
    +---main
    |   +---java
    |   |   \---com
    |   |       \---lds
    |   |           \---exito
    |   |               \---servicesdomain
    |   |                   \---nombre_aplicacion
    |   |                       |   ServicesDomainApplication.java
    |   |                       |   ServletInitializer.java
    |   |                       |
    |   |                       +---application
    |   |                       |   +---exceptions
    |   |                       |   |
    |   |                       |   \---usescases
    |   |                       |
    |   |                       +---domain
    |   |                       |   +---exceptions
    |   |                       |   |
    |   |                       |   +---models
    |   |                       |   |
    |   |                       |   \---ports
    |   |                       |       +---input
    |   |                       |       |
    |   |                       |       \---output
    |   |                       |
    |   |                       +---infraestructure
    |   |                       |   +---adapters
    |   |                       |   |   +---input
    |   |                       |   |   |
    |   |                       |   |   \---output
    |   |                       |   |
    |   |                       |   +---configuration
    |   |                       |   |       OpenApiConfig.java
    |   |                       |   |
    |   |                       |   \---exceptions
    |   |                       |
    |   |                       \---utils
    |   |
    |   \---resources
    |       |   application.properties
    |       |   logback.xml
    |       |
    |       +---static
    |       \---templates
    \---test
        \---java
            \---com
                \---lds
                    \---exito
                        \---servicesdomain
                            \---base_exito_services_domain
                                    ServicesDomainApplicationTests.java
```

### Seguridad

Desde el concepto de las seis capas de defensa, implementar protocoles que abarquen protección en los niveles de:

Aplicación:

- validación de entradas
- control de versiones en la gestión de dependencias
- Análisis del código estático con herramientas como el SONAR.
- Manejo adecuado de errores y excepciones.
- Observabilidad

Datos:

- Cifrado
- Copias de seguridad y respaldo
- Clasificación de datos según su sensibilidad (ofuscación)

### Reingenieria de procesos

Pensar si se da la oportunidad, en reestructurar algunos subprocesos y conjunto de datos buscando la optimización, jerarquía y tipo dentro del universo de la operación.

## Definición

La Arquitectura de servicios de dominio (DSA) es una agrupación de componentes basada en el patrón de microservicios y que en su conjunto atienden distintas operaciones especiales derivadas de la venta del sistema POS. Cada uno de estos componentes tiene una función definida y estructurada con el fin de hacerlos:

- Escalables
- Mantenibles
- Reutilizables
- Desacoplados
- Cohesivos

## Servicios, Componentes y Adaptadores

Son las unidades de software básicas en que se compone la DSA. Sus definiciones en el contexto de esta arquitectura son:

- ___Servicios___: Son todas aquellas piezas de software desplegadas e instanciadas sobre un servidor de aplicaciones u otro mecanismo y que están a la escucha de peticiones para realizar una tarea especifica sin importar quien lo invoque.

- ___Componentes___: Son todas aquellas unidades funcionales propias que se integran directamente dentro de otra aplicación y se ejecutan en el mismo contexto.

- ___Adaptadores___: Son todas aquellas piezas de software que actúan como un intermediario entre dos tecnologías, permitiendo que sistemas o componentes propios y de terceros trabajen juntos sin acoplarse.

## Capas de la Arquitectura de Servicios de Dominio

La DSA se divide básicamente en dos capas: la primera atiende la operación del lado de la terminal y son componentes que por su vocación deben operar lo mas cerca posible del sistema POS y de la misma terminal. La segunda capa es el conjunto de componentes que se enfocan en proveer una funcionalidad a varias terminales cliente y que pueden ser desplegados en sistemas distribuidos.

### Capa de Terminal

En esta capa encontramos los siguientes piezas de software:

- ___Terminal Gateway Service___: Su tarea es recibir y redireccionar las peticiones que lleguen desde el sistema POS cliente y entregar al mismo el resultado de la operación.
  
- ___Data Collector Service___: Su tarea es recolectar y validar la información fuente para el procesamiento de la operación. Recibe una petición y mediante una configuración definida para la operación a procesar, realiza la validación de data ademas de buscar la información complementaria que no sea entregada en la petición del POS cliente.

- ___Parameter Local Storage Service___: Su tarea es proveer de manera local las configuraciones definidas para el procesamiento de los datos que se usan en una operación.

- ___Java-Basic Adapter (JBAdapter)___: Es un servicio que cumple la tarea de "adaptar" la comunicación bidireccional entre el POS cliente y la DSA. Recibe la petición del lado del POS, traduce la petición entrante a la estructura esperada en la API del Terminal Gateway Service y realiza el mismo proceso en la respuesta que entregue la DSA.

- ___Toshiba Adapter___: Este adaptador es un componente expone una serie de funcionalidades propias del POS que son utilizadas del lado de la DSA sin acoplar la arquitectura a esta tecnología concreta. Provee por ejemplo interacción con la capa del frontend y recuperación de datos que tiene como fuente el sistema POS.

### Capa de Servicios

(en construcción)

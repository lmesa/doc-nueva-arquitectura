# Backlog

- [ ] Resolver comunicaciones hacia el autorizador (primer alternativa con dummies)
- [ ] Considerar los parámetros que son datos pero que hacen parte de la situación actual (hch - fwdcon - fwvdc)
- [ ] Incluir LdsMiddleware en el flujo
- [ ] Reunion con el equipo de desarrollo para socializar definiciones
- [ ] Revisar capitulo de encolamiento y reversos (Andrés)
- [ ] Seguridad (Alexis)
  - [ ] transacciones
  - [ ] Protección de las estructuras de las configuraciones
- [ ] Definir estrategia de componente BASIC (Mauro)
  - [ ] Se crea un nuevo componente
  - [ ] Detallar y definir llamados estándar

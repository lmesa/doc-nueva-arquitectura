# Definiciones

## 24-05-2024

<figure markdown="span">
    ![imagen](assets/definiciones/Diagrama%202-SOA.png)
    <figcaption>Detalles</figcaption>
</figure>

## 29-05-2024

<figure markdown="span">
    ![imagen](assets/definiciones/Mayo28-Derecho.jpg){ width="300" }
    <!-- <figcaption>Detalles</figcaption> -->
</figure>

1. Pruebas de `webFly` y `jetty` sobre la terminal
2. Se mantienen los llamados `java-basic`
3. Implementar estrategia de  notificación a BASIC cuando JAVA no responda, para evitar bloqueos en la terminal. Se pueden emplear Pipe para efectos de controlar  dicho escenario.
4. reducir/optimizar los llamados Java-basic.
5. Prueba de concepto en un ambiente nativo haciendo todo el flujo en el contexto de 4690/Sky: Los datos viajan vía AEF, es decir,  es vocación de basic entregar datos java, no por el call java basic, para que esta data se refleje en java ( lo llamamos contexto de la TRX). El call java basic sigue operando pero su vocación es solo hacer peticiones a java (formato json), y recibir respuestas del contexto de ejecución de Java (Ej: Venta ok, Venta Nok, pago ok, Pago Nok, etc.).

```mermaid
sequenceDiagram
    participant basic as BASIC
    participant api as Java-BASIC API
    participant aef as AEF
    participant cj as Componente-Java
    basic->>api: UserExit
    basic->>aef: XML
    api->>cj: RMI
    cj->>aef: request
    aef-->>cj: response
    alt Trx OK
        cj-->>api: response
        api-->>basic: response
    else Trx NOK
        cj-->>api: response
        api-->>basic: response
    end
    basic-xbasic: continua operación
```

6. Se construirá un/os componentes  (parte llamada adaptadores en la imagen) los cuales están en el contexto de la plataforma de Toshiba para "convertir" la data para el nuevo modelo (input), dentro de las responsabilidades de este adaptador esta: Captura de la información ingresados por el usuario (control del IO processor) Almacenamiento en el TLOG Impresión (reportes) Etc...

7. Los llamados java basic deberían ser iguales a las user Exits de SA?....

    <figure markdown="span">
        ![imagen](assets/definiciones/Mayo28-Izquierdo.jpg){ width="300" }
        <!-- <figcaption>Detalles</figcaption> -->
    </figure>

__Siguientes pasos:__

Hacer una PoC del flujo de basic a java (AEF) para disponibilizar los datos de la trx en lo que llamamos contexto de la TRX en un entorno nativo Toshiba.
Crear llamado java-basic en donde basic genera el formato de petición y respuesta en json
Crear componente(s) en java para hacer la captura, mostrar datos, etc.  desde java y un componente basado en servicio que resuelva una lógica particular por ejemplo un cálculo sencillo con datos de la TRX contexto,

## 18-06-2024

Grafico que muestra una primera exploracion de los datos y sus fuentes dentro del conmtexto de la operacion:

<figure markdown="span">
    ![imagen](assets/definiciones/MM-Diccionario.svg){ width="600" }
    <!-- <figcaption>Detalles</figcaption> -->
</figure>

```mermaid
classDiagram
    class PosTrx {
        -code: String
        -key: String
        -status: String
        -operación: String
        -date: String
        -terminal: String
        -transacción: String
        -cons: String
        -store: String
        -posFields: List<PosField>
        -active: String
        -indiceSecuencias: HashMap<Object, Object>
    }

    class PosField {
        -name: String
        -value: String
        -properties: Properties
        -persist: boolean
    }

    PosTrx "1" -- "0..*" PosField : contains
```

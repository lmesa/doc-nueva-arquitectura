# Estructura de carpetas

El esquema de carpetas utilizado por los componentes de la DSA se fundamenta principalmente en la **Arquitectura Hexagonal**, con el objetivo de garantizar una **separación clara de responsabilidades**. Esto facilita el mantenimiento, la evolución y la comprensión del código. El orden y la organización se alinean con principios de diseño como la **inversión de dependencias** y el **aislamiento de los detalles de implementación** respecto al núcleo de la aplicación. Es asi que podríamos enumerar los beneficios clave del esquema como:

1. **Responsabilidad bien definida por capa**:
   Cada capa del sistema tiene un propósito específico, lo que mejora la claridad y la estructura.

2. **Evita dependencias circulares o acoplamiento excesivo**:
   Se minimizan las relaciones innecesarias entre módulos, manteniendo la flexibilidad y modularidad.

3. **Aislamiento del núcleo del dominio**:
   El núcleo de reglas de negocio permanece independiente de frameworks, bibliotecas externas o tecnologías específicas.

4. **Facilidad de evolución y reemplazo**:
   Los componentes pueden ser modificados o sustituidos sin impactar negativamente otras partes de la aplicación.

5. **Mejora en las pruebas**:
   La separación clara facilita la escritura de pruebas unitarias e integración, reduciendo la complejidad de las mismas.

6. **Organización intuitiva**:
   Estructura los elementos de manera que los desarrolladores puedan navegar fácilmente y ubicar lo que necesitan con rapidez.

```plaintext title="Estructura de carpetas"
──src
    ├──main
    │   ├──java
    │   │   └──com
    │   │       └──lds
    │   │           └──exito
    │   │               └──domainservices
    │   │                   └──parameterslocalstorageservice
    │   │                       ├──application
    │   │                       │   ├──exceptions
    │   │                       │   ├──pipeline
    │   │                       │   │   └──stages
    │   │                       │   └──usecases
    │   │                       ├──domain
    │   │                       │   ├──dictionaries
    │   │                       │   ├──exceptions
    │   │                       │   ├──models
    │   │                       │   └──ports
    │   │                       │       ├──input
    │   │                       │       └──output
    │   │                       ├──infrastructure
    │   │                       │   ├──adapters
    │   │                       │   │   ├──input
    │   │                       │   │   └──output
    │   │                       │   ├──configuration
    │   │                       │   └──exceptions
    │   │                       └──utils
    │   └──resources
    └──test
        └──...
```

En el anterior gráfico se muestra la estructura estándar de carpetas que se crea mediante el arquetipo `base-services-dsa-archetype`. A continuación se detallan su estructura:

**GroupId:** El orden de carpetas inicial para todos los proyectos debe seguir la estructura: `src\main\java\com\lds\exito\domainservices\nombre-del-componente`. Al nombrar un nuevo componente, es importante agregar un sufijo que indique su propósito, como `service` (para servicios) o `adapter` (para adaptadores). Esto proporciona un contexto más claro sobre la función del componente al equipo de desarrollo.

**Carpeta de pruebas unitarias:** Los tests del proyecto se ubican en `\src\test`, siguiendo el mismo esquema definido en el `groupId`.

**Application:** Esta carpeta contiene los componentes responsables de orquestar la lógica del dominio. Se encargan de interactuar con los casos de uso, procesar entradas y salidas, y coordinar la interacción entre el dominio y los adaptadores. Esta capa actúa como intermediaria entre el dominio (entidades y lógica de negocio) y los puertos de entrada y salida.

1. **usecases:** Incluye los casos de uso del componente y las excepciones de la capa, las cuales se manejan y envían al `ControllerAdviser`.
2. **pipeline:** Contiene las clases e interfaces necesarias para implementar el patrón Pipeline.

**Domain:** Esta capa alberga los modelos, clases de valor y puertos, proporcionando una conectividad agnóstica con las capas externas.

1. **dictionaries:** Contiene datos constantes y enumeraciones utilizadas en el programa.
2. **models:** Incluye la clase `Message`, que extiende de la superclase `BaseMessage`, además de otras clases o entidades requeridas para la operación del componente.
3. **ports:** Contiene las interfaces que actúan como puertos de entrada y salida para la comunicación con los adaptadores.

**Infrastructure:** Esta capa agrupa los elementos relacionados con la implementación de los puertos de salida y las adaptaciones a tecnologías externas, como bases de datos, servicios de terceros, sistemas de mensajería o interfaces de usuario. Se comunica con el dominio mediante interfaces (puertos) y proporciona implementaciones concretas para estos, adaptándose a las infraestructuras tecnológicas requeridas.

1. **adapters:** Contiene las implementaciones específicas de los puertos de entrada y salida.
2. **configuration:** Incluye configuraciones globales del proyecto, como `beans` o el `ControllerAdviser`.

**Resources:** Agrupa los archivos `.properties` necesarios para la configuración externa del proyecto.
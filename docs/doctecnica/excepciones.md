# Manejo de excepciones

El manejo de excepciones es una práctica clave en el desarrollo de software que permite garantizar la estabilidad, legibilidad y mantenibilidad de las aplicaciones frente a errores o situaciones inesperadas. A continuación, se detalla su importancia y consideraciones clave dentro de la DSA:

## ¿Para qué sirve el manejo de excepciones?

**Prevención de fallos críticos:**

- Permite capturar errores en tiempo de ejecución antes de que generen interrupciones graves en el sistema.
- Ayuda a evitar la caída completa de la aplicación, permitiendo una recuperación controlada.

**Mejora de la experiencia del usuario:**

- Proporciona mensajes claros y útiles cuando ocurre un error, evitando que el usuario reciba información técnica confusa o sin sentido.

**Facilita la depuración:**

- Al registrar o capturar información relevante sobre las excepciones, los desarrolladores pueden analizar y resolver los problemas de manera más eficiente.

**Modularidad y robustez:**

- Ayuda a encapsular errores dentro de módulos específicos, reduciendo el impacto en otras partes del sistema.

**Cumplimiento de flujos de negocio:**

- Garantiza que los procesos críticos sigan un comportamiento predecible, incluso en situaciones excepcionales.

## Flujos Esperados  

Dentro de cualquier arquitectura o sistema, los flujos operativos suelen responder a uno de tres escenarios principales:  

### Happy Path (Camino Feliz)  

Es el flujo ideal, donde todo funciona correctamente y el sistema cumple su objetivo de manera esperada. Los datos son válidos, las reglas de negocio se respetan y no se presentan problemas técnicos. Este es el flujo que normalmente se diseña y prueba primero, ya que refleja el comportamiento estándar del sistema bajo condiciones óptimas.  

### Violación de Reglas de Negocio o Usuario  

Se produce cuando se infringen las reglas establecidas en el dominio del negocio o las políticas del sistema. Estas situaciones están **dentro de los escenarios esperados** y, por lo tanto, son manejadas explícitamente en los flujos del sistema.

Ejemplos comunes incluyen un usuario intenta realizar una acción para la cual no tiene permisos o el envío de datos que no cumplen con las validaciones definidas (e.g., campos obligatorios vacíos, formatos incorrectos).  

En estos casos, el sistema responde con mensajes claros que informan al usuario sobre la regla violada y cómo puede corregir el error. **No se consideran excepciones técnicas**, ya que forman parte de las reglas previstas del sistema y están diseñadas para manejarse sin interrupciones críticas.  

### Excepción Técnica o de Código  

- Este escenario representa fallos imprevistos en el sistema causados por problemas técnicos. Las excepciones técnicas son **errores no deseados** que están fuera del control directo del sistema o del usuario.  
- Ejemplos incluyen:
  - Errores de conexión con una base de datos.  
  - Nulidad de un objeto no controlada (e.g., `NullPointerException` en Java).  
  - Tiempo de espera agotado al consumir un servicio externo.  
- Estas situaciones deben capturarse mediante mecanismos de manejo de excepciones para evitar caídas abruptas del sistema. Esto permite:
  - Implementar una recuperación controlada.  
  - Notificar adecuadamente al usuario o al equipo técnico.  

Por lo anterior es crucial diferenciar el tratamiento de los flujos _no-happy path_, ya que lanzar excepciones indiscriminadamente puede ser perjudicial para el sistema. Cuando se lanza una excepción, el sistema debe recolectar información para construir la pila de errores (stack trace), lo que implica un consumo considerable de recursos. Además, las excepciones interrumpen abruptamente el flujo de ejecución, lo que puede tener consecuencias no deseadas en sistemas complejos o distribuidos.  

## Manejo de Excepciones en la DSA

Cada capa principal de la DSA tiene su excepción declarada en su carpeta `exceptions`. Es asi que encontraremos tres clases llamadas `DomainException`, `ApplicationException` e `InfrastructureException`. Estas clase extienden una superclase abstracta llamada `BaseException` que provee dos constructores base: `BaseException(String message)` y `BaseException(String message, Throwable cause)`. La clase `ApplicationException` presenta una excepción que se tratara mas adelante.

```java title="Ejemplo de DomainException"
public class DomainException extends BaseException {

    public DomainException(String message, Throwable cause) {
        super(message, cause);
    }

    public DomainException(String message) {
        super(message);
    }

}
```

### Excepciones en la capa de aplicación (Application Layer)

Con la clase `ApplicationException` se debe considerar implementar un constructor adicional que no se hereda de la super clase. Este nuevo constructor recibe dos argumentos de tipo `Throwable` y `BaseMessage`, ademas de implementar un método `getter` llamado `getBaseMessage()`. Esta clase esta monitorizada por la clase `GlobalControllerAdvice` y en la lógica de este se extrae el `Message` de la operación para componer la respuesta adecuada hacia el sistema cliente, trasportando el error raíz por toda la cadena de servicios y asi poder identificar el componente donde se presentó la excepción.

```java title="Ejemplo de ApplicationException"
public class ApplicationException extends BaseException {

    private BaseMessage baseMessage;

    // constructor adicional
    public ApplicationException(Throwable cause, BaseMessage baseMessage) {
        super(cause.getMessage(), cause);
        this.baseMessage = baseMessage;
    }

    public ApplicationException(String message) {
        super(message);
    }

    // método adicional
    public BaseMessage getBaseMessage() {
        return baseMessage;
    }

}
```

#### Flujo de las excepciones entre servicios

Dado que en servicios distribuidos es crucial conocer donde se generó la excepción raíz, es necesario manejar un mecanismo de transporte de errores entre componentes para que la información fuente sea conocida por el sistema cliente.

<figure markdown="span">
    ![Flujo de una excepción](documentacion/flujo-excepciones.png)
    <figcaption>Flujo de una excepción</figcaption>
</figure>

Se busca mediante el un `ControllerAdviser` el manejo de estas excepciones encadenadas para garantizar el transporte hasta el servicio cliente a traves de del flujo de operación.

### Excepciones y Trazabilidad

Dado que estas excepciones son técnicas y no deben ser visibles en detalle para el sistema cliente pero si para el equipo técnico interno, estas se lanzan acompañadas de un registro de logging basado en `SLF4J` para almacenar información detallada sobre las excepciones. Esto es crucial para la trazabilidad y el diagnóstico.

El siguiente es un ejemplo del uso de excepciones de la DSA:

```java title="Bloque ejemplo de manejo de errores"
...
catch (ResourceAccessException ex) {

    log.error(LogMessage.create(
        traceId, 
        TGS001.getError(), 
        TGS001.getDescription(), 
        ex.getMessage()) 
        );

    throw new InfrastructureException(TGS001.getError()); 
    // Excepción de la capa de infraestructura

}
```

Nótese que el uso de las excepciones debe ir acompañado de su respectivo registro en los logs. Esto es fundamental, ya que, aunque las excepciones permiten gestionar errores de ejecución y notificarlos al sistema cliente, este no debe exponer detalles técnicos sensibles al usuario final. La responsabilidad de capturar y almacenar dicha información recae en la trazabilidad operativa, que, mediante un proceso paralelo, se encarga de registrar los detalles relevantes para el diagnóstico y la solución del problema.

```json title="Respuesta de error al cliente"
{
    "operationType": "SELL",
    "subOperationType": "SUBSCRIPTION",
    "traceId": "fb4bbc82-beef-43dc-9f34-4d858cd52c99",
    "messageStatus": "ERROR",
    "dataSection": {
        "sections": {
            "errorMessage": "TGS001: SERVICIO NO DISPONIBLE"
        }
    }
}
```

```plaintext title="Registro en logs del error"
2024-12-11 09:35:52.745 [http-nio-8080-exec-2] 
ERROR [DataCollectorAdapter.java:84] 
- [TraceId: fb4bbc82-beef-43dc-9f34-4d858cd52c99 
- Error: TGS001: SERVICIO NO DISPONIBLE 
- Descripción: Servicio Data Collector no disponible 
- Detalle: I/O error on POST request for "http://localhost:8081/api/v1/": 
Connect to localhost:8081 [localhost/127.0.0.1, localhost/0:0:0:0:0:0:0:1]]
```

para complementar el tema, revise el capitulo de  [trazabilidad operativa](trazabilidad.md#trazabilidad-operativa)

## ¿Qué se debe tener en cuenta al manejar excepciones?

**Evitar capturar excepciones genéricas innecesariamente:**

- Utilizar excepciones específicas en lugar de capturar `Exception` o `Throwable`, ya que esto puede ocultar errores importantes.

**Mantener mensajes claros y significativos:**

- Al lanzar excepciones, asegurarse de incluir información relevante sobre el problema para facilitar la resolución.

**No abusar de excepciones para flujos normales:**

- Las excepciones son para situaciones inesperadas, no para manejar condiciones regulares de negocio.

**Usar bloques `try-catch` de forma específica:**

- Limitar los bloques `try-catch` al código donde realmente puede ocurrir una excepción. Esto facilita la lectura y evita capturas innecesarias.

**Registrar las excepciones (Logging):**

- Utiliza herramientas de registro para almacenar información sobre las excepciones.

**No suprimir excepciones sin manejarlas:**

- Si se captura una excepción, asegurarse de manejarla adecuadamente o relanzarla si no se puede resolverla en el contexto actual.

**Definir excepciones personalizadas:**

- Para contextos específicos del negocio, crear excepciones propias que representen claramente el error y su impacto.

**Limpieza de recursos:**

- Asegurarse de cerrar conexiones, liberar memoria o manejar recursos en un bloque `finally` o utilizando estructuras como `try-with-resources`.

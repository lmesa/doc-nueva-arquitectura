# Trazabilidad Operativa

La **trazabilidad operativa** es la capacidad de rastrear y registrar las actividades y eventos que ocurren durante la ejecución de un sistema o aplicación. Se enfoca en el seguimiento del flujo de datos, acciones y procesos en tiempo real o en el historial de operación, con el objetivo de garantizar que el sistema funcione correctamente, identificar problemas y facilitar la auditoría.

## Principales características

- **Monitoreo en tiempo real**: Rastrear qué ocurre en el sistema en el momento en que sucede.
- **Registro de eventos**: Capturar información relevante, como peticiones, respuestas, excepciones, y estados del sistema.
- **Seguimiento de transacciones**: Identificar cómo una acción específica (como una solicitud de usuario) fluye a través de diferentes componentes del sistema.
- **Diagnóstico y resolución de problemas**: Permite identificar la causa raíz de errores o cuellos de botella en el sistema.
- **Auditoría y cumplimiento**: Mantener un historial de operaciones para cumplir con normativas o revisiones.

## Trazabilidad en la DSA

En la DSA se usa `SLF4J` (mediante anotaciones de `Lombok`)  como interfaz y `Logback` como motor de logging. Esto permite escribir código desacoplado del framework de logging específico, facilitando la flexibilidad para cambiarlo sin modificar el código fuente.

Con la finalidad de homogenizar la estructura de las trazas de error en los logs, se provee a través del la librería `DataCatalogLibrary` una clase `LogMessage` con un único método estático llamado `create`, el cual recibe como parámetros 4 argumentos que son:

- **traceId**: el identificador único del mensaje que está en la cabecera del mismo.
- **error**: El código de error interno del componente que se haya definido en la clase `ErrorCode` de la capa `domain`.
- **description**: La descripción asociada al código de error.
- **detail**: El detalle del error lanzado por la excepción (`exception.getMessage`) o la descripción técnica del desarrollador.

El siguiente es un ejemplo de registro de logging en el código:

```java title="Bloque ejemplo de manejo de logging"
...
catch (ResourceAccessException ex) {

    log.error(LogMessage.create(
        traceId, // el identificador del Message
        TGS001.getError(), // El código interno de error
        TGS001.getDescription(), // description asociada al código de error
        ex.getMessage()) // El detalle técnico del error
        );

    throw new InfrastructureException(TGS001.getError());

}
```

Nótese que el bloque `log.error` va seguido de un `throw` que lanza la excepción específica de la capa correspondiente, incluyendo el mismo código de error utilizado en el log. Esta práctica se emplea para garantizar que los registros de log sean consistentes con el mensaje enviado al sistema cliente, lo que facilita tanto la identificación del error como la del servicio donde ocurrió el problema.

Además, esta consistencia entre los logs y las excepciones mejora la trazabilidad del sistema, ya que permite correlacionar rápidamente los eventos registrados con los errores reportados. Esto resulta clave en entornos distribuidos o con múltiples servicios, donde la identificación precisa de la fuente del error es esencial para agilizar el diagnóstico y la resolución del incidente.

```json title="Respuesta de error al cliente"
{
    "operationType": "SELL",
    "subOperationType": "SUBSCRIPTION",
    "traceId": "fb4bbc82-beef-43dc-9f34-4d858cd52c99",
    "messageStatus": "ERROR",
    "dataSection": {
        "sections": {
            "errorMessage": "TGS001: SERVICIO NO DISPONIBLE"
        }
    }
}
```

```plaintext title="Registro en logs del error"
2024-12-11 09:35:52.745 [http-nio-8080-exec-2] 
ERROR [DataCollectorAdapter.java:84] 
- [TraceId: fb4bbc82-beef-43dc-9f34-4d858cd52c99 
- Error: TGS001: SERVICIO NO DISPONIBLE 
- Descripción: Servicio Data Collector no disponible 
- Detalle: I/O error on POST request for "http://localhost:8081/api/v1/": 
Connect to localhost:8081 [localhost/127.0.0.1, localhost/0:0:0:0:0:0:0:1]]
```

para complementar el tema, revise el capitulo de  [manejo de excepciones](excepciones.md#manejo-de-excepciones-en-la-dsa)

## La clase ErrorCode

Se ha destacado en varias ocasiones la importancia del uso de códigos de error internos en cada servicio. En la ubicación `domain/dictionaries` se encuentra la clase `ErrorCode`, que es de tipo `Enumerador` y tiene la responsabilidad de almacenar los códigos de los posibles errores que puedan ocurrir durante la ejecución del servicio. Esta estructuración proporciona una manera ordenada y homogénea de gestionar los fallos del sistema, permitiendo una comunicación clara y eficaz de los errores, lo cual facilita la detección, el diagnóstico y la resolución oportuna de los problemas.

Además, el uso de un enumerador para los códigos de error asegura consistencia y facilita el mantenimiento del sistema, ya que centraliza la definición de los errores y evita posibles discrepancias o redundancias en diferentes partes del código.

```java title="Ejemplo de la clase ErrorCode en Terminal Gateway Service"
public enum ErrorCode {

    TGS001("TGS001: SERVICIO NO DISPONIBLE", "Servicio Data Collector no disponible"),
    TGS002("TGS002: ERROR DEL PROCESO", "Respuesta del servicio consultado con novedad"),
    TGS003("TGS003: ERROR EN LOS PARÁMETROS", "El tipo y el subtipo de la operación no pertenecen al mismo contexto"),
    TGS004("TGS004: ERROR DEL PROCESO", "Falló la deserialización"),
    TGS005("TGS005: ERROR DEL PROCESO", "Respuesta vacía");

    private String error;
    private String description;

    private ErrorCode(String error, String description) {
        this.error = error;
        this.description = description;
    }

    public String getError() {
        return error;
    }

    public String getDescription() {
        return description;
    }

}
```

Se recomienda que cada servicio contenga una clase-enumerador de este tipo para guardar consistencia en el manejo de errores a traves de la DSA y evitar descripciones de errores poco detallados, ambiguos o demasiado verbosos.

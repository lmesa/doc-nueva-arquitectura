# Introducción

Para el intercambio de información entre componentes internos y externos, se diseñó un objeto de valor denominado `BaseMessage`. Su propósito es proporcionar un `wrapper` de datos consistente y transversal para toda la información que circula dentro de la arquitectura. A continuación, se detalla su estructura y uso:

## Cabecera o _Header_

La cabecera (header) del mensaje consta de cuatro elementos:

### OperationType

Define el tipo de operación al que pertenece el mensaje, clasificándolo en uno de los tres grupos principales: _venta_, _no venta_ o _pago_. Es inmutable.

### SubOperationType

Especifica las operaciones detalladas dentro de cada tipo de operación. Estas guían a la DSA en el flujo a seguir e indican los datos complementarios necesarios para cada caso. Es inmutable.

### TraceId

Es un identificador único utilizado en sistemas distribuidos para rastrear el recorrido de una solicitud a través de múltiples servicios o componentes. Su propósito es simplificar el monitoreo y la depuración en entornos complejos. Este identificador debe generarse al inicio de la solicitud, ya sea por el cliente o por el adaptador de entrada de la arquitectura. Se utiliza el formato estándar UUID (Universally Unique Identifier) para garantizar unicidad y consistencia en el sistema. Es inmutable.

### MessageStatus

Es un enumerador que indica el estado actual del mensaje, con tres posibles valores: `OK`, `NOK` y `ERROR`.

- **OK**: Estado inicial del mensaje al ser creado. Si el flujo no presenta inconvenientes, permanecerá en este estado hasta su finalización (_happy path_).

```json title="Representación de un mensaje OK"
{
  "operationType": "NOSELL",
  "subOperationType": "RECHARGE",
  "traceId": "fb4bbc82-beef-43dc-9f34-4d858cd52c99",
  "messageStatus": "OK",
  "dataSection": {
    "sections": {
      "payload": {
        "date": "20240725",
        "amount": "10000",
        "terminalId": "0160",
        "storeId": "0013",
        "transactionId": "0003",
        "plu": "115192",
        "cashierId": "0000011111",
        "time": "144557",
        "customer": ""
      }
    }
  }
}
```

- **NOK**: Indica una inconsistencia en el flujo debido a una violación prevista de las reglas de negocio. En este caso, el mensaje es marcado para ser ignorado en los procesos posteriores hasta la conclusión del flujo.

```json title="Representación de un mensaje NOK"
{
  "operationType": "SELL",
  "subOperationType": "SUBSCRIPTION",
  "traceId": "fb4bbc82-beef-43dc-9f34-4d858cd52c99",
  "messageStatus": "NOK",
  "dataSection": {
    "sections": {
      "errorMessage": "PLS002: ERROR DEL PROCESO"
    }
  }
}
```

- **ERROR**: Representa una condición técnica inesperada, como una excepción. Este estado provoca que el flujo sea interrumpido, y el error es devuelto como una `Exception` mediante la clase `GlobalControllerAdvice`.

```json title="Representación de un mensaje ERROR"
{
  "operationType": "SELL",
  "subOperationType": "SUBSCRIPTION",
  "traceId": "fb4bbc82-beef-43dc-9f34-4d858cd52c99",
  "messageStatus": "ERROR",
  "dataSection": {
    "sections": {
      "errorMessage": "DCS001: SERVICIO NO DISPONIBLE"
    }
  }
}
```

## Cuerpo o _DataSection_

El `DataSection` del mensaje es una estructura mutable diseñada para transportar la carga útil de una operación específica. Su organización se basa en **secciones** con formato clave-valor (`Map<String, Object>`), lo que permite agregar diferentes tipos de datos, cada uno identificado por una clave única. Funciona como un **contenedor de información** que incluye pequeños **contenedores rotulados** llamados `section`, capaces de almacenar tanto objetos específicos como otros contenedores.

Si observamos el json de la **representación de un mensaje OK**, vemos que contiene una _sección_ llamada `payload` la cual contiene a su vez un conjunto de datos. Esta sección es principal y debería contener los datos principales de cualquier petición hecha a la nueva arquitectura exceptuando en los casos donde el mensaje toma el estado `NOK` o `ERROR` en cuyo caso la sección se llamará `errorMessage`.

Las `section`, al ser estructuras de tipo _clave-valor_, deben mantener consistencia en sus nombres cuando son transportadas entre diferentes servicios. Para garantizar esta consistencia y facilitar el mantenimiento, se recomienda evitar "quemar" las claves directamente en el código (hardcoding). En su lugar, estas claves deben definirse como constantes centralizadas en la clase `BaseMessage` del componente `DataCatalog`, como ocurre con los ejemplos `payload` y `errorMessage`.

Es crucial gestionar eficientemente el contenido del `DataSection`, asegurando que los mensajes sean _suficientes_ en términos de información contenida. Esto implica evitar duplicidad de datos, eliminación de información obsoleta para operaciones futuras y reducir el anidamiento innecesario.

La clase `BaseMessage` proporciona múltiples métodos para realizar operaciones _CRUD_ sobre estos contenedores, facilitando su manipulación y organización.

!!! tip
    En el caso de los adaptadores, el **Mensaje** como objeto de transporte recibe un tratamiento especial, ya que estos adaptadores, por su naturaleza, deben comunicarse con las capas externas y con terceros en términos de sus respectivas APIs. Esto ocurre cuando no es posible que dichas capas o sistemas externos se adapten al esquema del **Mensaje** definido por la DSA. Los adaptadores tienen la responsabilidad de actuar como traductores entre las tecnologías externas y la DSA.

## Uso de la clase BaseMessage

La clase `BaseMessage` se encuentra en la librería `datacatalog`, la cual es importada por todos los proyectos que componen la DSA. Esto garantiza el uso consistente de las mismas entidades y estructuras de datos en todo el flujo de los procesos.

Se recomienda extender esta clase en una clase `Message` dentro de la capa de dominio de los componentes, en lugar de utilizarla directamente. De esta forma, se facilita el manejo de diferentes tipos de mensajes con un núcleo común, adaptado según el contexto.

La clase `BaseMessage` dispone de dos constructores: uno vacío, utilizado para procesos de serialización/deserialización, y otro que acepta tres argumentos: `operationType`, `subOperationType` y `traceId`. Al instanciar la clase, su variable `messageStatus` toma por defecto el valor `OK`.

La clase cuenta con dos métodos:

- `setNOKStatus(String message)`: Cambia el estado del mensaje para que sea administrado por el flujo como error de regla de negocio.
- `setErrorStatus(String message)`: Cambia el estado del mensaje para que sea administrado por el flujo de excepciones.

Una vez creado el objeto `Message`, se debe agregar la `dataSection`. Esta clase también se encuentra en la librería `datacatalog` y contiene un atributo de tipo `HashMap<String, Object>`. `DataSection` proporciona un conjunto de métodos para su manipulación:

- `DataSection()`: Constructor vacío que inicializa el atributo `HashMap<String, Object>`.
- `DataSection(String sectionName, Object data)`: Constructor con argumentos que inicializa el atributo `HashMap<String, Object>` y crea una `section` dentro del mapa.
- `addSection(String sectionKey, Object data)`: Agrega una `section` a un `dataSection` existente.
- `getSection(String sectionKey, Class<T> type)`: Recupera una `section` del `dataSection`, usando la clave y especificando el tipo de retorno esperado.
- `getSections()`: Recupera todas las `sections` que contiene el mensaje.
- `deleteSection(String sectionKey)`: Elimina una `section` específica.
- `deleteAllSections()`: Elimina todas las `sections` del `dataSection`.
- `toString()`: Representación en cadena del objeto.

!!! warning
    Se debe tener en cuenta que cualquier cambio realizado en uno de los componentes de la librería `datacatalog` afectará a todos los componentes que la importen. Por lo tanto, es importante realizar ajustes o cambios con cuidado para evitar impactos inesperados en otros módulos que dependan de ella.

# Componentes

La **Arquitectura de Servicios de Dominio (DSA)** se basa en una estructura modular enfocada en [arquitectura limpia](arquitectura-limpia.md#arquitectura-limpia) que organiza los distintos elementos necesarios para implementar y ejecutar sus funcionalidades de manera orgánica y desacoplada. Estos componentes trabajan de manera independiente pero colaboran entre sí para cumplir con los objetivos del negocio, asegurando escalabilidad, flexibilidad y mantenibilidad.

## Módulos de la DSA

A continuación, se describen los principales componentes de la arquitectura:

### Servicios

Los **servicios** son unidades independientes y autónomas de software, cada una diseñada para ejecutar una función específica dentro de la DSA. Cada servicio se enfoca en una tarea particular del negocio, siguiendo principios clave como **alta cohesión** (responsabilidad bien definida) y **bajo acoplamiento** (interacción mínima con otros servicios). Los servicios están diseñados para ser escalables, independientes y fácilmente reemplazables, lo que facilita la evolución de la arquitectura sin afectar el sistema global.

### Adaptadores

Los **adaptadores** son componentes especializados en gestionar la comunicación entre la DSA y tecnologías o sistemas externos, como APIs de terceros, bases de datos, servicios de mensajería o sistemas legados. Funcionan como una capa intermedia, que abstrae y encapsula la lógica necesaria para interactuar con estos sistemas, proporcionando una interfaz estándar y desacoplada para el resto de los componentes internos. Los adaptadores son esenciales para garantizar que las integraciones externas no afecten directamente a los servicios internos, permitiendo una mayor flexibilidad y mantenibilidad del sistema.

### Librerías

La **librería** es una colección de funciones, clases o recursos predefinidos que proporcionan funcionalidades específicas y son transversales al funcionamiento de los **servicios** y **adaptadores** dentro de la DSA. Los módulos están diseñados para ser reutilizables y encapsulan comportamientos comunes que pueden ser utilizados por diferentes componentes, asegurando consistencia y reduciendo la duplicación de código en toda la arquitectura.

## Esquema de la DSA

La **Arquitectura de Servicios de Dominio (DSA)** se segmenta en dos grupos principales: uno ubicado en el lado de las terminales **(DSA-INTERNA)** y otro en las plataformas externas a las terminales **(DSA-EXTERNA)**. La principal diferencia entre estos grupos radica en el comportamiento de sus componentes, determinado por la naturaleza del entorno en el que operan.

Por ejemplo:

- **En las terminales**, los flujos de trabajo son **sincrónicos** y se basan en una **única petición por operación**.
- **En las plataformas externas**, los flujos son predominantemente **asíncronos** y manejan **múltiples peticiones simultáneamente**.

Estas diferencias llevan a que los componentes de ambos lados, aunque compartan un propósito común, sean diseñados, configurados y desplegados teniendo en cuenta las particularidades de su entorno. Esto asegura que cada grupo de componentes funcione de manera óptima dentro de su contexto específico.

## DSA Interna

<figure markdown="span">
    ![DSA-INTERNA](documentacion/DSA-INTERNA.png)
    <figcaption>DIAGRAMA C4 DSA-INTERNA</figcaption>
</figure>

<figure markdown="span">
    ![DSA-INTERNA-detalle](documentacion/DSA-INTERNA-detalle.png)
    <figcaption>Detalle DSA-INTERNA</figcaption>
</figure>

Esta capa de la DSA es la encargada de la recolección y normalización de las peticiones que llegan del lado del sistema POS cliente, interactuando con componentes de terceros y usuarios para la creación de la petición que sera enviada a la capa superior.

### Detalle de la DSA Interna

La DSA interna está conformada por los siguientes componentes:

#### **Adaptadores**

- **JBAdapter**: Componente de tipo adaptador encargado de interpretar y traducir las peticiones y las respuestas entre la DSA y el POS Cliente.

<figure markdown="span">
    ![JBAdapter](documentacion/jbadapter.png)
    <figcaption>Detalle interno JBAdapter</figcaption>
</figure>

- **POSAdapter**: Componente de tipo adaptador-librería que provee métodos para interactuar con el cliente POS.

#### **Servicios**

- **Terminal Gateway Service**: Encargado de enrutar las solicitudes entre los componentes DSA ubicados en el lado de la terminal y realizar peticiones a componentes por fuera de la misma.

<figure markdown="span">
    ![TerminalGatewayService](documentacion/terminalgateway.png)
    <figcaption>Detalle interno TerminalGatewayService</figcaption>
</figure>

- **Data Collector Service**: Responsable de recolectar información complementaria, normalizar los paquetes de datos para continuar con las operaciones, y validar la información recibida.  

<figure markdown="span">
    ![DataCollectorService](documentacion/datacollector.png)
    <figcaption>Detalle interno DataCollectorService</figcaption>
</figure>

- **Parameters Local Storage Service**: Persiste localmente las configuraciones obtenidas desde el almacén principal, garantizando la disponibilidad de los parámetros requeridos.

<figure markdown="span">
    ![ParametersLocalStorageService](documentacion/parameterlocal.png)
    <figcaption>Detalle interno ParametersLocalStorageService</figcaption>
</figure>

#### **Librerías**

- **Data Catalog Library**: Librería que contiene estructuras, entidades y datos transversales a toda la arquitectura, proporcionando un marco común para los distintos componentes.

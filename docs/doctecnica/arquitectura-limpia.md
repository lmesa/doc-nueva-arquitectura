# Arquitectura Limpia

Uno de los principales retos en la construcción de sistemas informáticos es mantener la independencia entre sus funcionalidades. Con el tiempo, a medida que estos sistemas crecen y se vuelven más complejos, tienden a acoplarse, convirtiéndose en monolitos difíciles de mantener. Este problema ha sido abordado en diversas ocasiones por la comunidad, que ha propuesto distintas soluciones encapsuladas bajo el concepto de **Arquitectura Limpia**. Este enfoque de diseño se basa en la creación de **sistemas modulares, mantenibles y fácilmente escalables**, con el objetivo principal de separar las preocupaciones y garantizar que las dependencias del sistema estén dirigidas hacia el núcleo de la aplicación, donde reside la lógica de negocio.

Las soluciones derivadas de la Arquitectura Limpia comparten un principio clave: **las dependencias deben fluir de las capas exteriores hacia el núcleo del sistema, y no al revés**. Es decir, se busca proteger la lógica de negocio de cualquier tipo de dependencia hacia tecnologías externas o de terceros, manteniéndola aislada de los detalles técnicos. Esto no solo facilita el mantenimiento y la evolución del sistema, sino que también asegura que el negocio pueda adaptarse rápidamente a cambios tecnológicos sin comprometer su funcionamiento central.

## Arquitectura limpia en la DSA

En la DSA, se busca un enfoque que promueva la separación de responsabilidades tanto en la relación entre los componentes como en la construcción interna de cada uno de ellos, sin necesariamente adherirse de forma estricta a un patrón definido. Sin embargo, en este contexto, se utiliza como referente **la Arquitectura Hexagonal o Arquitectura de Puertos y Adaptadores**, la cual propone una visión clara de separación entre la lógica del negocio y el resto del sistema a través de los siguientes conceptos:

- **Puerto**: Es una interfaz que define un conjunto de operaciones o funcionalidades que el núcleo de la aplicación espera recibir o proporcionar, facilitando la comunicación con el mundo exterior (como bases de datos, interfaces de usuario, sistemas externos, etc.). El puerto establece un **contrato** entre el núcleo de la aplicación (la lógica de negocio) y los adaptadores que interactúan con el exterior. Los puertos especifican qué acciones son posibles sin detallar cómo se implementan estas acciones. Son independientes de cualquier tecnología específica, lo que permite que, al cambiar la infraestructura o tecnología externa, solo se necesiten ajustes en los adaptadores, sin afectar la lógica de negocio del núcleo.

- **Adaptador**: Es un componente que implementa un puerto para conectar el núcleo de la aplicación con el mundo exterior. Los adaptadores son responsables de traducir y adaptar las interacciones entre el sistema interno (el núcleo) y las tecnologías externas (como bases de datos, servicios web, interfaces de usuario, etc.). Implementan los puertos definidos en el núcleo, permitiendo que las funcionalidades del sistema se conecten con componentes externos. Además, convierten los datos entre el formato entendido por el núcleo de la aplicación (dentro de la lógica de negocio) y el formato requerido por la tecnología externa. Los adaptadores permiten que el sistema se adapte fácilmente a nuevas tecnologías o herramientas, ya que solo es necesario cambiar el adaptador cuando se sustituye una tecnología o se agregan nuevas funcionalidades externas.

Es asi como vemos en la estructuración de la [DSA-INTERNA](componentes.md#dsa-interna) que en los "bordes" de la arquitectura, la interacción entre componentes externo se da a traves de **adaptadores** los cuales son los únicos componentes dentro de la arquitectura que deben conocer e interactuar con otros servicios que no estén integrados en la DSA.

## Construcción de componentes

Para la construcción de componentes se provee un arquetipo de maven llamado `base-services-dsa-archetype`. Este arquetipo provee la estructuración básica de un proyecto basado en arquitectura de puertos y adaptadores tal como se puede ver en el capitulo de [estructuración de carpetas](estructura-carpetas.md#estructura-de-carpetas). Si plasmamos este patron en el flujo de un componente y su relación entre componentes internos vemos una clara separación de responsabilidades y desacoplamiento entre el núcleo del sistema y sus dependencias externas:

<figure markdown="span">
    ![TerminalGatewayService](documentacion/datacollector.png)
    <figcaption>Ejemplo de estructuración interna del  TerminalGatewayService</figcaption>
</figure>

El componente se divide en tres capas principales que son _domain, application e infrastructure_. Vemos en la capa superior o mas externa los componentes (azul claro) de tipo adaptador que implementan (en gris) los puertos de tipo interfaz y que existen en la capa mas interna o inferior con el fin de que sean accesibles por las capas superiores. Estos puertos a su vez son usados como _fachada_ por clases concretas que contienen la lógica del negocio (azul oscuro).

Vemos asi como tanto a nivel de componentes como del componente en si, se adopta el enfoque de arquitectura de puertos y adaptadores sin acoplarse dogmáticamente a la literatura del patrón y solo siendo usado como guía conceptual.

# Persistencia

A continuación, se tratarán las particularidades de la persistencia en la DSA.

## Terminal

### Persistence Service

Este componente es el servicio encargado de gestionar la persistencia del lado de la terminal mediante **JPA e Hibernate**. Expone un endpoint que permite el consumo de la base de datos y la ejecución de los procesos **CRUD** necesarios para manejar la información relevante de la transacción.

Dado que su base de datos es embebida, se requieren configuraciones específicas para su correcto funcionamiento.

| Propiedad | Descripción | ¿Para qué sirve? |
|-----------|------------|------------------|
| `spring.jpa.hibernate.ddl-auto=update` | Actualiza la estructura de la BD sin borrar datos. | Útil en desarrollo. En producción, es mejor usar `validate` o `none`. |
| `hibernate.connection.shutdown=true` | Ejecuta `SHUTDOWN` al cerrar la aplicación. | Evita bloqueos en **HSQLDB**. |
| `spring.datasource.hikari.maximum-pool-size=1` | Limita el número de conexiones activas a **1**. | Previene bloqueos en **HSQLDB** en modo archivo. |
| `spring.datasource.hikari.minimum-idle=1` | Mantiene **1 conexión abierta** en el pool. | Evita cierres innecesarios de la base de datos. |
| `spring.datasource.hikari.auto-commit=false` | Desactiva el auto-commit en las transacciones. | Útil si se usa `@Transactional` para control manual de transacciones. |

### Base de Datos

Del lado de la terminal, se maneja una persistencia con **HSQLDB v2.4.1** en modo _standalone_, ubicada en:

📂 `/cdrive/f_drive/TerminalServices/DB`.

El proceso de despliegue es el siguiente:

1. **Ubicar el JAR de HSQLDB**  
   - Copiar `hsqldb.jar` en `/cdrive/f_drive/TerminalServices/DB`.

2. **Iniciar la base de datos**  
   - Abrir una terminal en la misma ubicación y ejecutar:  
  
   ```sh
      java -cp hsqldb.jar org.hsqldb.server.Server -database.0 file:<nombre_del_esquema> -dbname.0 <nombre_del_esquema> --port 9091
   ```

    📌 *`--port` define el puerto para conexiones remotas cuando sea necesario.*

3. **Conectarse a la base de datos**  
   - Usar el gestor de base de datos de preferencia para configurar la conexión.

<figure markdown="span">
    ![img](documentacion/config-pers-term.png)
    <figcaption>Configuración para la conexión remota administrativa</figcaption>
</figure>

!!! note "Nota"
    Por defecto, el usuario administrador es `SA` sin contraseña.

Ya conectado a la BD, se procede a ejecutar los siguientes scripts:

1. Creación de tablas y constraints.
2. Creación de índices (si aplica).
3. Creación de un nuevo usuario con contraseña.
4. Otorgar permisos de **DBA** al nuevo usuario.
5. Eliminación del usuario `SA` (opcional, si se creó otro usuario con permisos administrativos).
6. Apagar la base de datos mediante el comando:

    ```sql
    SHUTDOWN;
    ```

!!! warning "Importante"
    Es de suma importancia que al finalizar los ajustes necesarios en la BD y teniendo en cuenta que una base de datos embebida, se termine ejecutando el comando `SHUTDOWN;` dado que este aplica los cambios y cierra de manera correcta la BD impidiendo que la misma quede bloqueada.

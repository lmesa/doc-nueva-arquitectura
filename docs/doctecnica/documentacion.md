# Documentación

La **Arquitectura de Servicios de Dominio (DSA)** es un modelo estructurado de componentes interrelacionados que sigue los principios de arquitectura limpia basada en microservicios. Su propósito principal es integrar servicios complementarios a la operación central del negocio, promoviendo un bajo acoplamiento y una alta cohesión entre ellos.

Surge como respuesta a la necesidad de reestructurar los componentes previamente organizados bajo un modelo monolítico, que dificultaba el mantenimiento y la integración de nuevas funcionalidades, generando retrasos en los tiempos de desarrollo e inconsistencias de funcionamiento en producción.

La DSA define una base estructurada para el desarrollo e integración de funcionalidades complementarias, guiándose por las siguientes directrices:

1. **Alta cohesión**: Cada componente o módulo debe estar especializado en una única funcionalidad específica del negocio.
2. **Bajo acoplamiento**: Los componentes deben comunicarse mediante un mecanismo estándar, garantizando relaciones agnósticas entre ellos.
3. **Multientorno**: Siempre que sea posible, los componentes deben tener capacidades de despliegue en múltiples backends.
4. **Orientación a la configuración**: Estandarizar la integración para facilitar la incorporación de nuevos servicios.
5. **Autonomía**: Minimizar la interacción al intercambio esencial de mensajes, asegurando la independencia de cada servicio.
6. **Independencia tecnológica**: Reducir al mínimo la dependencia de tecnologías externas o servicios fuera del core del negocio.
7. **Principios de desarrollo**: Implementar y observar principios como SOLID, KISS y DRY, junto con las buenas prácticas de CLEAN CODE.
8. **Flexibilidad**: Dar prioridad a la adaptabilidad por encima de dogmas estrictos.
9. **Pruebas unitarias**: Garantizar la calidad del código mediante pruebas automatizadas.
10. **Documentación técnica**: Asegurar que todos los servicios estén bien documentados.
11. **Cultura de desarrollo**: Promover disciplina y buenas prácticas frente a la improvisación o informalidad.

Estas directrices aseguran que la DSA sea una arquitectura robusta, escalable y alineada con las necesidades del negocio.

Otra gran ventaja de la arquitectura es que, aunque en sus bases se construye con lenguaje JAVA, es susceptible de interactuar con componentes escritos en otros lenguajes siempre y cuando se respeten los contratos entre las API's.

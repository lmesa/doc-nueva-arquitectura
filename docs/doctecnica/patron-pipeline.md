# Patrón Pipeline

Dentro de la construcción de los componentes se han tenido en cuenta el uso de patrones de diseño de software, que permitan orden y escalabilidad. Es asi como dentro de la estructura superior se implementa el **patron pipeline**. Este patrón se utiliza para procesar datos en una serie de etapas o pasos secuenciales, donde el resultado de cada etapa se pasa como entrada a la siguiente.

## Concepto básico

El patrón Pipeline organiza un flujo de trabajo en el que los datos (o eventos) se _"canalizan"_ a través de una serie de _"etapas"_ que realizan transformaciones específicas. Cada etapa del pipeline procesa los datos y pasa el resultado a la siguiente etapa en la cadena. Este enfoque permite una mayor modularidad y reutilización, ya que las etapas pueden ser independientes entre sí.

Sus principales características son la secuencialidad, la transformación y la modularidad.

## Implementación

La implementación de este patrón se lleva a cabo en la capa _application_. En la carpeta `pipeline` se define:

- **Interfaz Pipe**: La interfaz `Pipe` define un contrato para cada etapa o stage del pipeline, y el método `add` permite agregar nuevas etapas, creando un flujo de procesamiento en serie.

```java title="Definición del Pipe"
public interface Pipe<I, O> {
    // Método que define la transformación de un tipo de entrada I a un tipo de salida O.
    O runStage(I inputMessage);

    // Método por defecto para encadenar esta etapa del pipeline con otra.
    default <N> Pipe<I, N> add(Pipe<O, N> pipe) {
        // Este método permite añadir una nueva etapa al pipeline, devolviendo una nueva instancia de Pipe
        // que toma el tipo de entrada I, pasa por esta etapa (runStage) y luego pasa el resultado a la siguiente etapa (pipe.runStage).
        return inputMessage -> pipe.runStage(runStage(inputMessage));
    }
}

```

- **Etapas (Stages)**: En la carpeta `pipeline/stages` se definen las clases correspondientes a cada una de las etapas que se procesarán en el flujo del pipeline. Estas clases concretas deben implementar la interfaz `Pipe`, especificando el tipo de datos que esperan recibir y el tipo de datos que entregarán. Actúan como puntos de entrada hacia los casos de uso, desde donde se pueden ramificar hacia otras clases, siempre asegurando que estén agrupadas de manera cohesiva. Este enfoque facilita la identificación de puntos específicos dentro del flujo, evitando la creación de un código desorganizado o difícil de mantener.

```java title="Ejemplo de un Stage"
@Service
@RequiredArgsConstructor
public class DataCollectorStage implements Pipe<Message, Message> {

    private final DataCollectorPort dataCollector;

    @Override
    public Message runStage(Message inputMessage) {

        if (inputMessage.getMessageStatus() == MessageStatus.NOK)
            return inputMessage;

        try {
            return dataCollector.getDataFromCollector(inputMessage);
        } catch (RuntimeException ex) {
            throw new ApplicationException(ex.getMessage(), inputMessage);
        }
    }

}
```

- **Pipeline y PipelineImpl**: Pipeline es una interfaz que provee un único método de inicialización del procesos del `pipe`. Esta interfaz debe ir en la capa de `domain` idealmente para desacoplar su implementación de las capas superiores y facilitar las pruebas unitarias.

```java title="Ejemplo"
public interface Pipeline {

    public Message executeMainProcess(Message inputMessage);
}
```

PipelineImpl es la implementación concreta del puerto Pipeline y en este se definen los `stages` y el orden de ejecución de los mismos mediante el método `add`.

```java title="Ejemplo"
@Slf4j
@Service
@RequiredArgsConstructor
public class PipelineImpl implements Pipeline {

    // se definen los stages a encadenar
    private final MessageValidatorStage messageValidatorStage;
    private final DataCollectorStage dataCollectionStage;
    private final PostProcessClientStage postProcessClientStage;

    public Message executeMainProcess(Message inputMessage) {

        // se encadenan siguiendo el orden necesario
        Pipe<Message, Message> pipeline = messageValidatorStage
                .add(dataCollectionStage)
                .add(postProcessClientStage);

        // ejecuta los stages de manera secuencial.
        return pipeline.runStage(inputMessage);

    }

}
```

!!! warning
    Se debe observar que en la declaración del `Pipe pipeline` el tipo de dato de entrada debe corresponder al tipo de entrada del primer stage ejecutado (`messageValidatorStage`) y el dato de salida debe corresponder al dato de retorno del ultimo stage en la cadena de procesamiento (`postProcessClientStage`).

## Conclusión

Como se puede observar, el principal beneficio del patrón **Pipeline** radica en su **modularidad** y **flexibilidad** en el procesamiento de datos. Al descomponer un proceso en una serie de etapas (o filtros) independientes, cada una encargada de realizar una transformación o acción específica, este patrón actúa como el núcleo principal de la aplicación. A partir de él, se pueden ramificar procesos especializados que complementen o extiendan una etapa o _stage_ en particular.

Sin embargo, aunque el patrón Pipeline es altamente efectivo en diversos escenarios, no es adecuado para todos los casos. Algunos ejemplos donde no resulta apropiado son: **procesos no secuenciales o dependientes**, **tareas demasiado simples o triviales**, **procesos que requieren un estado compartido o transacciones**, y **procesamiento paralelo no soportado**.

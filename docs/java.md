# JavaBasic Adapter

<figure markdown="span">
    ![img](assets/java/DS-JavaBasicVia.svg)
    <figcaption>Flujo de la información desde BASIC a N.Arq</figcaption>
</figure>

<figure markdown="span">
    ![img](assets/java/DS-JavaBasicAdapter.svg)
    <figcaption>detalle de la comunicación</figcaption>
</figure>

<figure markdown="span">
    ![img](assets/java/DS-Timer.svg)
    <figcaption>detalle de la comunicación</figcaption>
</figure>

documento: SO_Programming Guide.pdf (pag 334 - 520)

Componente encargado de las comunicaciones entre el servicio `TerminalGateway` y `Java-BASIC API`.

`F:` Esta en Linux mientras que `C:` y `M:` necesitan clases con la terminación `4690`, ejemplo `FileInputStream4690`.

Archivos no VFS y no NFS limitan el nombramiento a 8.3, por lo archivos con nombres mas largos son truncados silenciosamente.

El sistema de archivos VFS admite nombres de archivo de hasta 256 caracteres de longitud.

|Package|Descripción|
|-|-|
|com.ibm.OS4690|Paquete para clases que proporcionan acceso a varias características del sistema operativo en controladores y terminales de tienda. Estas características incluyen archivos POS, archivos keyed, pipes, estado del sistema y controles del sistema.|
|com.ibm.OS4690.jiop.util|Paquete de procesador de entrada/salida de Java para clases de utilidad, como Monitor del Sistema y Depuración.|
|com.ibm.OS4690.jiop|Paquete para clases que proporcionan funciones de procesamiento de entrada/salida en Java.|

| Método                            | Explicación                                                                                  | Patrones SOLID que cumple              | Patrones SOLID que viola                      | Impacto en el rendimiento del equipo         | Nivel de acoplamiento                        |
|-----------------------------------|----------------------------------------------------------------------------------------------|----------------------------------------|------------------------------------------------|----------------------------------------------|---------------------------------------------|
| **Llamadas directas**             | Invocación directa de métodos entre clases                                                   | Cumple: SRP, OCP, LSP                  | Viola: Ninguno                                  | Muy bajo                                    | Alto                                         |
| **Eventos y bus de eventos**      | Comunicación mediante eventos asincrónicos                                                   | Cumple: SRP, OCP, DIP                  | Viola: ISP                                     | Moderado                                    | Bajo                                         |
| **Inyección de dependencias (DI)**| Gestión de dependencias mediante un contenedor DI                                            | Cumple: SRP, DIP                       | Viola: Ninguno                                  | Bajo                                        | Bajo                                         |
| **Memoria compartida/colas locales**| Comunicación directa mediante memoria compartida o colas dentro del mismo proceso           | Cumple: SRP                            | Viola: DIP, OCP                                 | Muy bajo                                    | Moderado                                     |
| **RPC (Remote Procedure Call)**   | Comunicación mediante llamadas de procedimiento remoto, optimizadas para red local           | Cumple: SRP, DIP                       | Viola: Ninguno                                  | Moderado                                    | Moderado                                     |
| **Reflexión**                     | Uso de reflexión para invocar métodos dinámicamente dentro del mismo proceso                 | Cumple: Ninguno                        | Viola: SRP, OCP, LSP                            | Moderado                                    | Bajo                                         |
| **Comunicación HTTP**             | Comunicación mediante llamadas HTTP, usualmente entre procesos o servicios                   | Cumple: SRP, DIP                       | Viola: ISP, LSP                                 | Alto                                        | Muy bajo                                     |

## Log desde Programación Orientada a Aspectos (AOP)

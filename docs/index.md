# Arquitectura de Servicios de Dominio

## Contexto

Ante la necesidad de migrar los servicios restantes que conviven en el modelo de Framework, se abre el debate sobre el proceso de implementación de los mismos del lado del modelo de Dominio presentándose tres caminos:

- Migración como se ha estado llevando hasta hoy.
- Refactorización del modelo de Dominio.
- Proponer una evolución del modelo de Dominio, aplicando buenas prácticas de la industria del desarrollo de software. (Dominio 2.0)

Este último camino coge fuerza dada la oportunidad de implementar una ___[arquitectura orientada a Microservicios](https://www.atlassian.com/es/microservices/microservices-architecture)___ que provea al conjunto de componentes miembros una operatividad desacoplada, reusable, mantenible y orientada a la configuración.

## Generalidad

<figure markdown="span">
    ![clifre](assets/index/001.svg)
    <figcaption>Diagrama de alto nivel</figcaption>
</figure>

___Objetivo___: Una arquitectura de capas basado en Microservicios donde se promuevan unos lineamientos de desarrollo y cultura que al ser observados en los proyectos a futuro, promueven la modularización de los servicios enfocándose en la responsabilidad única, comunicación agnóstica y buenas practicas en el desarrollo. (Sistemas tipo LEGO).

# Venta Contenido

## Capítulos

### Entrada de datos desde el cliente

#### Payload del lado de Basic

Para venta de contenido general llega la siguiente información:

<figure markdown="span">
    ![imagen](assets/ventas-contenidos/cab-vent-recarga.png){ width="600" }
    <figcaption>Datos generales recarga</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/ventas-contenidos/body-vent-recarga.png){ width="600" }
    <figcaption>Datos complementarios recarga</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/ventas-contenidos/cab-vent-general.png){ width="600" }
    <figcaption>Datos generales general</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/ventas-contenidos/body-vent-general.png){ width="600" }
    <figcaption>Datos complementarios general</figcaption>
</figure>

#### Payload del lado del Dominio

<figure markdown="span">
    ![imagen](assets/ventas-contenidos/body-vent-general-dominio.png){ width="600" }
    <figcaption>Datos complementarios general</figcaption>
</figure>

#### Atributos

##### Producto

- nombre (String)
- PLU (String)
- EAN (String)
- hchId (String)
- tipoContenido (String)s
- codigoProducto (String)
- permiteAnulacion (boolean)
- permiteConsulta (boolean)
- referencia (Referencia)
- valor (Valor)
- voucher (Voucher)

#### Referencia

- solicitaReferencia (boolean)
- expresionRegular (String)
- iLongitud (int)
- iPrefijo (int)

#### Valor

- expresionRegular (String)

#### Voucher

- titulo (String)
- PieVoucherInicial (String)
- PieVoucherFinal (String)

#### Almacén de Datos

|Dato|Descripción|
|-|-|
|AdditionalData|Información adicional que retorna un pago o item [Solo para tarjetas propias]|
|AdditionalFeatures|Caracteristicas adicionales del item o del pago|
|addNewVariablesToReport|Caracteristicas adicionales del ítem o del pago|
|Amount|Valor de el pago, item o nosale|
|AnulationStatus|Identifica el Estado de la Anulacion|
|AnulationType|Identifica el Tipo de Anulacion|
|AuthEndProduct|parametro para indicar si el authend se realizara por producto|
|AuthorizationBus|Identifica el bus de autorizaciones que debe usar el producto|
|AuthorizationClassName|Identifica la clase para la autorizacion.|
|AuthorizationDetails|Mapa de detalles de la autorización|
|AuthorizationIsRequired|Indica si requiere autorizacion el item|
|AuthorizationRequired|Identifica si se debe de autorizar alguna operacion|
|AuthorizationStatus|Identifica el estado de la autorizacion|
|AuthorizationType|Identifica el tipo de autorizacion|
|AuthorizationValidatorClassName|Identifica la clase validador de respuestas en autorización.|
|AuthorizerPort|Puerto del autorizador|
|AuthOrReverse|Identifica si la operacion fue exitosa al autorizar o hacer el reverso|
|AvailablePoints|Puntos disponibles|
|BillableData|Billable|
|BillingModality|Código de modalidad de facturación|
|BinTp|Codigo del barcode capturado para tarjetas propias|  
|ButcheryCommandBarcode|Barcode that identifies the butchery command|
|CardNumber|Card number|
|CashierId|Numero de identificación del cajero|
|CashierName|Nombre del cajero|
|CATALOG|identificador tarjeta regalo con catalogo|
|ClassNameQueryAuthEndProductName|clase por defecto para la consulta de authend|
|ClifredCed|Dato de ClifredCed|
|ClifredOn|Dato de ClifredOn|
|consecutiveStatus|Identifica bloqueo por consecutivo fiscal|
|CONVENIO|Identificador del convenio para la tarjeta regalo con catalogo|
|CustomerBalance|Saldo disponible del cliente|
|CustomerDetails|Mapa de detalles del customer|
|CustomerEmployeeDiscountId|Identificador de descuento del cliente|
|CustomerId|Numero de identificacion del cliente|
|CustomerName|Nombre del cliente|
|customerQueryRappi|Consulta del CustomerQuery de RappiPayless|
|CustomerType|Tipo de cliente|
|CustomerTypeId|Tipo de documento del cliente|
|CustomerVentaStaff|Tipo trx de venta|
|custumerDetails|Mapa de detalles del customer|
|DataAdminVariables|Variables capturadas en las operaciones no sale Administrativas|
|DataCaptureVariables|Variables solicitadas en el datacapture|
|Date|Dato de fecha|
|Dependence|Dependencia|
|DescriptorTag|Indica el tag del descriptor. Este tag se usa para reconocer el tipo de descriptor PaymentItemDescription, TransactionRecord o LogRecord|
|Device|Identificador de dispositivo externo|
|DigitalReceiptIndicator|Tirilla Digital|
|DocumentType|Simple document type|
|ElectronicJournals|IDs de descriptores para registrar en el log de soporte electronico|
|ElectronicLogRecord|UserString generado para pago o item|
|Email|Correo electronico|
|EndDate|Fecha final de la resolución|
|EndRange|Rango final del consecutivo fiscal|
|Event|Codigo de evento, nosale|
|EventBolsillo|Dato de evento bolsillo|
|EventCod|Dato telefono|
|EventCodeQuota|Codigo evento Descuento Empleado|
|EventData|Dato de evento, nosale|
|EventName|Dato nombre de evento|
|ExpiricyDatePoints|Fecha de expiracion de los puntos|
|ExternalApprovalCode|Código de aprobación de autorizador externo|
|FiscalConsecutiveNumber|Consecutivo fiscal|
|FiscalNumber|Numero fiscal|
|FiscalTicketNumber|Numero de tikete fiscal|
|Footers|Lista de IDs para vouchers tipo footer (atados a la tirilla)|
|Frank|franqueo|
|Frank|String de franqueo|
|HabeasDataIndicator|Estado del habeas data|
|IdDevueltas|Variable que guardara la cedula para devueltas monedero|
|IndTypeValidation|Inidicador tipo de validacion a realizar|
|InitialRange|Rango inicial del consecutivo fiscal|
|InputType|Input method|
|InternalOperationVersion|Variable para identificar en los llamados entre capas, la version de la operacion a realizar|
|IsAddNewVariableToReports|Valida si debe agregar nuevas variables al PaymentDetail|
|IsJoinableTrace|Indica si el mensaje de una autorización se debe unir con varios ítems (no aplica para pagos)|
|IsRecoveryConsult|Indica si la transaccion de recovery es solo de consulta|
|ItemDescription|Descripcion del item|
|ItemDetails|Mapa de detalles del item|
|ItemDiscountIva|Descuento por iva diferencial|
|ItemExtension|Extension funcional del item|
|ItemExtensionAlternative|Extension funcion alterna del item|
|ItemPrice|Precio del item|
|ItemQuantity|Cantidad del item|
|ItemType|modo de venta del item|
|Journal|Journal en el login de la terminal|
|Key|Indica un id de tecla que se está ejecutando en el momento del pago|
|LastStatus|Último status retornado al servicio externo|
|LenMax|OUTGOIN|
|LenMin|OUTGOIN|
|LineDescriptor|Descriptor para el item o el pago|
|Max|OUTGOIN|
|Min|OUTGOIN|
|MonthsValidity|Meses de validación de la resolución|
|NegativeElectronicJournals|IDs de descriptores para registrar en el log de soporte electronico|
|NegativeFooters|Lista de IDs para vouchers tipo footer (atados a la tirilla)|
|NegativeFrank|franqueo negativo|
|NegativePaymentItemDescription|Descripcion del medio de pago para la tirilla|
|NegativeRecoveryRecord|IDs de descriptor de recuperacion de registro de operacion extendida en la transaccion|
|NegativeTraceIdentificator|Identificador del mensaje negativo para autorización|
|NegativeTransactionRecords|IDs de descriptores de registro de operacion en la transaccion|
|NegativeVouchers|Lista de IDs para vouchers tipo voucher (no atados a la tirilla)|
|NewVlr|Valor nuevo capturado para responderle a basic|
|NoSaleOperation|Nombre de operacion de no venta|
|NoSaleOperationDetails|Detalle de la operacion de no venta|
|NoSaleOperationIdentificator|Identificador del registro de la operacion de no venta para recovery|
|Nut|Nut de la operacion realizada, solo aplica para retornos desde la capa de dominio|
|OgEvent|Primer codigo de evento, nosale|
|OMSCustomerId|OMS customer ID|
|OMSDocumentType|OMS document type|
|OperatorId|login del cajero|
|orderId|Order ID entregado por RappiPayless|
|OriginalTerminalNumber|Numero de la terminal|
|OriginalTransactionNumber|Numero de la transaccion|
|PasswordIndicator|Estado de la clave del cliente|
|paymentAmmount|Valor del pago realizado|
|paymentComplement|Complemento pago realizado|
|paymentDetails|Detalles extras del pago|
|PaymentItemDescription|Descripcion del medio de pago para la tirilla|
|paymentTender|codigo del medio de pago|
|Plu|Item identificator|
|PluDiscount|PluDiscount|
|PluName|Nombre de pruducto|
|PluPrice|plus con precio final|
|PluValueList|Lista de plu por valor para venta continua|
|PluVarietyPBR|Inidicador de Plu/Variedad para la tabla PRODUCTBUSINESSRULEs|
|Pocket|PocketID|
|PointsToExpire|Saldo puntos a Vencer|
|PositiveTraceIdentificator|Identificador del mensaje positivo para autorización|
|Prefix|Prefijo|
|PrimaryPhoneNumber|Numero telefonico primario del cliente|
|ProductName|parametro para indicar el nombre de un producto|
|ProductType|Identifica el tipo de producto|
|PromotionDetails|Nombre de operacion de no venta de promociones detalles|
|PromotionOperation|Nombre de operacion de no venta de promociones|
|QrIntentionId|Identifica la intención de compra con QR Bancolombia|
|qryRappi|Consulta de cliente para Rappy|
|RecoveryLogRecord|String de recuperacion generado para pago o item|
|RecoveryOriginalTerminalNumber|Numero de la terminal original para recovery|
|RecoveryOriginalTransactionNumber|Numero de la transaccion original para recovery|
|RecoveryRecord|IDs de descriptor de recuperacion de registro de operacion extendida en la transaccion|
|ReportArray|Reportes generados|
|ReportSection|Seccion del reporte (Footer o Voucher)|
|ReportString|Reporte generado|
|RequestHttpAuthorization|Mapa de parametros para authorizacion http: hostPrimary, portPrimary, uri, requestHttp|
|RequiresItemValidation|Requiere validar Item|
|ResolutionNumber|Número de resolución|
|ResolutionStatus|Estado de la resolución|
|ResponseAutorization|Identifica el estado de la autorizacion o del reverso|
|ResponseIsoTemplate|Plantilla a utilizar para respuestas de la mensajeria Iso|
|ResponseMap|Outgoing map|
|ResponseMessage|OUTGOIN|
|ResponseMessageList|Outgoing message list|
|ResponseOption|Outgoing option menu|
|ResponseStatus|Outgoing message status|
|ReverseStatus|Identifica el Estado del Reverso|
|ReverseType|Indentifica el Tipo de Reverso|
|SecondaryPhoneNumber|Numero telefónico secundario del cliente|
|SecurityIndicator|Indicador para saber si la tarjeta propia es con o sin seguridad|
|ServiceNumber|Variable que guardara el service number para monedero|
|Sign|Indica el signo de la operacion, si es TRUE es positiva y si es FALSE es negativa|
|SignS|Indica el valor de la operacion|
|StandaloneMaintenanceStatus|Estado de la validación del mantenimiento automático|
|StandaloneMaintenanceStatusMessage|Mensaje de estado de la validación del mantenimiento automático|
|StartDate|Fecha de inicio de la resolución|
|StatusClient|Estado del cliente|
|StoreName|Nombre de la tienda|
|StoreNumber|Numero de la tienda|
|Tender|código del medio de pago|
|TerminalNumber|Numero de la terminal|
|TicCardNumber|Numero de la tarjeta TIC|
|TotalBalance|Valor total restante por pagar de la transaccion|
|TransactionLogRecord|UserString generado para pago o item|
|TransactionNumber|Numero de la transaccion|
|TransactionRecords|IDs de descriptores de registro de operacion en la transaccion|
|TsProce|Identifica si estamos en un conex o en una venta normal|
|Type1x1|Dato tipo de 1x1 automático o manual|
|TypeRecovery|Identifica si el tipo de Recovery es por IPL o por Suspend|
|TypeReq|Identifica que tipo de consecutivo fiscal está activo|
|TypeRes|Tipo de captura a realizar en interacción JB en muspellheim|
|TypeValidation|Variable que guardara el tipo de validación a realizar|
|UE|User Exit|
|UpdateDate|UpdateDate|
|UserExit|Identifica la userexit donde se realiza el llamado|
|UserString|String secundario para el Tlog|
|ValidatorReportClassName|Identifica la clase validador para construcción de variables en los reportes.|
|ValueValidation|Valor que se utilizara según el tipo de validación|
|VarName|Dato nombre de valor|
|varName|Numero de código otp para pago|
|Vlr|Dato valor|
|Vouchers|Lista de IDs para vouchers tipo voucher (no atados a la tirilla)|

```yaml title="Payload Framework"
---
code: '128'
key: 
status: '0'
operacion: '0'
date: '19'
terminal: '180'
transaccion: '0001'
cons: 
store: '0013'
active: '1'
posFields:
  BsExclusive: '0'
  FiscalNumber: '1'
  DatEnt: '128'
  InTrx: '0'
  StoreName: EXT EXITO LAB
  IO: '0'
  EcrAmount: '0'
  CashierId: '22222'
  Payments: '0'
  Oper: '0000022222'
  UE: '08'
  PluQty: '1'
  Procedencia: '1'
  TsProce: "-1"
  ClifreOn: '0'
  Total: '0'
  PluName: RECARGA ABIERTA MO
  CashierName: PRUEBAS 2
  Plu: '274992'
  Trx: '0001'
  TypeReq: '0'
  VarName: '0013'
  Vlr: '1000'
  FiscalTicket: '00000000000001'
  operador: '22222'
  fecha: '190724'
  hora: '1119'
  horaint: '111954953'
indiceSecuencias:
  dato: 

```

```yaml title="Payload Dominio"
---
ItemQuantity: '1'
DataCaptureVariables:
  dato: 
ItemDiscountIva: '0'
CashierId: '0000022222'
OriginalTransactionNumber: '0001'
ItemExtension: '999'
ReverseStatus: NOAPP
ItemPrice: '50000'
SignS: '1'
ProductType: ITEM
TsProce: "-1"
ItemType: '999'
AuthorizationType: INMDT
ReverseType: NOAPP
IdDevueltas: '0'
OriginalTerminalNumber: '180'
AuthEndProduct: NOAPPLY
ItemDescription: COL NETFLIX $50.00
IndTypeValidation: POSVALIDATION
LastStatus: '1'
AnulationStatus: NOAPP
AnulationType: APPLY
Plu: '0000001126976'
ItemExtensionAlternative:
  dato: 
PluVarietyPBR: '999'
AuthorizationIsRequired: 'true'
Sign: 'true'
AuthorizationStatus: PNDNG

```

definir las transiciones de la maquina de estados usando un archivo de configuración externo y el uso de `spring-statemachine-core` version `3.2.0`

### Mecanismo de configuracion de datos

### Datos de entrada (desde POS)

```json
{
    "plu": "12345",
    "terminalId": "123",
    "transactionId": "1",
    "amount": "1000",
    "cashierId": "22222",
    "storeId": "0013",
    "date": "20240725",
    "time": "1900"
}

```

### Payload de salida (hacia POS)

```json title="response OK"
{
    "statusResponse": "0",
    "messageResponse": null
}

```

```json title="response NOK"
{
    "statusResponse": "99",
    "messageResponse": "lorem ipsum"
}

```

``` mermaid
sequenceDiagram
    autonumber
    participant bas as POS-Basic
    participant jpi as JavaApi
    participant lds as LdsMiddleware
    participant jba as JBAdapter
    participant tgs as TerminalGatewayService
    participant dcs as DataCollectorService
    participant pss as PersistenceService
    participant pls as ParametersLocalStorageService
    participant pos as POSAdapter
    participant dms as DataMapperService
    participant apa as ApproverAdapter
    participant apr as Servicio de Aprobador

    alt NoVenta
        Note over bas: Inicia venta con un EAN/PLU<br>basado en DataEntry<br><br>[Type:NoVenta]<br>[SubType:VentaContenido]

        bas->>jpi: Envía solicitud con intención de compra.<br>[trama]
        jpi->>lds: Redirige la solicitud hacia la arquitectura Java.<br>[trama]
        lds->>jba: Redirige la solicitud hacia el adaptador DSA.<br>[trama]
        Note over jba: Interpreta la trama y la traduce<br>en una estructura Message

        jba->>tgs: Envía datos en formato Message.<br>[JSON]
        Note over tgs: Reenvía la solicitud al servicio correspondiente.

        tgs->>dcs: Envía datos en formato Message.<br>[JSON]
        dcs->>pls: Solicita datos de validación<br>basados en la operación y el PLU.<br>[JSON]
        pls-->>dcs: Responde con los datos de validación<br>según las parametrizaciones.<br>[JSON]
        Note over dcs: Valida los datos recibidos<br>según las parametrizaciones.
        Note over dcs: Incorpora al mensaje la información<br>de validación de operación.
        Note over dcs: Verifica si se requieren datos<br>complementarios para completar la operación.

        alt Requiere datos complementarios

            loop Mientras haya datos pendientes
                dcs->>pos: Solicita interacción con el cajero<br>para capturar datos.
                Note over pos: Solicita los datos al cajero.
                pos-->>dcs: Envía los datos ingresados<br>por el cajero.
                Note over dcs: Valida la información recibida.

                alt Datos correctos
                    Note over dcs: Continúa a la siguiente iteración.
                else Datos incorrectos
                    Note over dcs: Notifica error en la digitación.<br>Permite reintentos si es necesario.
                    dcs->>pos: Solicita corrección al cajero.<br>(Máximo 3 reintentos)
                    pos-->>dcs: Envía los datos corregidos<br>por el cajero.

                    alt Datos corregidos correctamente
                        Note over dcs: Procede a la siguiente iteración.
                    else Reintentos fallidos
                        dcs-->>tgs: Devuelve respuesta NOK.<br>[JSON]
                        tgs-->>jba: Devuelve respuesta NOK.<br>[JSON]

                        Note over jba: Interpreta la respuesta<br>y la transforma a formato POS cliente.

                        jba-->>lds: Devuelve respuesta NOK.<br>[trama]
                        lds-->>jpi: Devuelve respuesta NOK.<br>[trama]
                        jpi--x bas: Devuelve respuesta NOK.<br>[trama]
                    end
                end
            end

        else No requiere datos complementarios
            Note over dcs: Procesa la solicitud sin datos adicionales.<br>[JSON]

            dcs-->>tgs: Devuelve respuesta OK.<br>[JSON]
            par persistencia de los datos de validación.
                tgs->> pss: Envía data para persistir.
                Note over pss: Persiste datos de transacción y asigna estado [PENDIENTE DE PAGO].
            and respuesta al POS para realizar el pago
                tgs-->>jba: Devuelve respuesta OK.<br>[JSON]
                Note over jba: Interpreta la respuesta<br>y la transforma a formato POS cliente.
            end

            jba-->>lds: Devuelve respuesta OK.<br>[trama]
            lds-->>jpi: Devuelve respuesta OK.<br>[trama]
            jpi-->> bas: Devuelve respuesta OK.<br>[trama]
            Note over bas: PLU vendido.
            Note over bas: Notificado de las reglas de operación del PLU.
        end

    else Pago

        Note over bas: registra el pago del producto
        Note over bas: [Type:Pago]<br>[SubType:VentaContenido]

        bas->>jpi: Envía confirmación de pago.<br>[trama]
        jpi->>lds: Traduce la confirmación de pago hacia la arquitectura Java.<br>[trama]
        lds->>jba: Redirige la confirmación de pago hacia el adaptador DSA.<br>[trama]
        Note over jba: Interpreta la trama y la traduce<br>en una estructura Message

        jba->>tgs: Envía datos en formato Message.<br>[JSON]
        Note over tgs: Reenvía la confirmación de pago al servicio correspondiente.

        tgs->>dms: Envía datos en formato Message.<br>[JSON]
        Note over dms: Transforma la confirmación de pago a formato ISO8583.

        dms->>apa: Envía datos en confirmación de pago.<br>[ISO8583]
        Note over apa: Consume API de aprobador.

        apa->>apr: Envía datos en confirmación de pago.<br>[ISO8583]
        apr-->>apa: responde activación OK.<br>[ISO8583]
        apa-->>dms: responde activación OK.<br>[ISO8583]
        Note over dms: Transforma la confirmación de pago de ISO8583 a formato JSON.

        dms-->>tgs: responde activación OK.<br>[JSON]
        tgs->> pss: Cambia estado transacción a [APROBADO].
        Note over pss: Cambia estado transacción a [APROBADO].

        tgs-->>jba: responde activación OK.<br>[JSON]
        Note over jba: Interpreta la respuesta<br>y la transforma a formato POS cliente.

        jba-->>lds: responde activación OK.<br>[TRAMA]
        lds-->>jpi: responde activación OK.<br>[TRAMA]
        jpi--x bas: responde activación OK.<br>[TRAMA]

    end
```
# Definiciones

<p align="center"><strong>“La única forma de ir rápido es hacerlo bien”</strong> - Uncle Bob -</p>

La definición de esta nueva arquitectura abarca la construcción y relación de los componentes que atenderán las operaciones de ___VENTA___, ___NO VENTA___ y ___PAGO___ siguiendo las especificaciones definidas por la dirección de SAV.

En términos de investigación, reto, implementación y desempeño, representa lo siguiente:

1. Construir soluciones que permitan fáciles despliegues en distintos ecosistemas (local - servidor - nube).
2. Acercamiento a arquitecturas ya probadas como son ___ELERA___ o similares.
3. Facilitar la implementación de tecnologías más modernas y adecuadas a los recursos existentes y futuros.
4. Mayor flexibilidad para implementar una arquitectura heterogénea pero ordenada.
5. Oportunidad de implementar estrategias de resiliencia del sistema.
6. Oportunidad de implementar estrategias de monitoreo eficaces y que comuniquen.
7. Baja dependencia de las limitantes de obsolescencia de software y hardware.
8. Implementación de estándares del mercado en cuanto a paradigmas de programación y patrones de diseño, estas como guías conceptuales - ___No hay bala de plata -___.
9. Posibilidad futura de contenerizar los servicios, lo que proporciona una mayor portabilidad, gobernanza y escalabilidad sobre los mismos.
10. Facilitar la implementación de medidas de seguridad más robustas y consistentes.
11. Mayor facilidad al añadir o actualizar componentes en un único punto centralizado cuando sea necesario.
12. Un ___servicio centralizado___ puede facilitar la integración con otros sistemas y servicios externos, mejorando la interoperabilidad.
13. Promover una arquitectura saludable desarrollando software de forma orgánica.
14. Fomentar la comprensión, la depuración y la modificación del código.
15. Ayudar al equipo de desarrollo a trabajar mas eficientemente
16. Gran objetivo: ___Mejorar algo que ya funciona___. (agregar valor).

## Directrices Generales

Para el diseño y construcción de la nueva arquitectura se debe tener en cuenta las siguientes orientaciones:

1. Sus módulos deben ser ___especializados___ en atender una y solo una funcionalidad especifica del negocio. (alta cohesión)
2. Deben proveer un mecanismo de comunicación estándar garantizando una relación agnóstica con el resto de componentes. (bajo acoplamiento)
3. Debe poseer en lo posible, características de ___despliegue en multiples backends___. (Multientorno)
4. Estandarización para una rápida integración de nuevos servicios. (Servicios orientados a la configuración)
5. Deben basar su interacción al ___intercambio mínimo___ de ___mensajes suficientes___ para realizar su labor, garantizando su independencia y autonomía.
6. Idealmente ___indice mas bajo de acoplamiento___ con tecnologías externas y/o servicios ajenos al core del negocio.
7. Observar la implementación de los principios ___SOLID___, ___KISS___ y ___DRY___ además de buenas practicas de ___CLEAN CODE___ como pilar fundamental de construcción del código.
8. ___Flexibilización___ sobre dogma.
9. Pruebas unitarias.
10. Documentación técnica.
11. ___Cultura de desarrollo___ (Disciplina y buenas prácticas) sobre informalidad e improvisación.

Estos son _grosso modo_ un buen punto de partida para iniciar el diseño y construcción.- ___ojo, no son dogma___ -. Es claro que el rumbo se va definiendo en el proceso de desarrollo.

## "Receta" propuesta para abordar la solución

- Definir el problema de manera abstracta.
- Pensar en la solución en términos monolíticos y luego descomponer.
- Representar adecuadamente las entidades del negocio.
- Diseñar la solución considerando la perspectiva del usuario no la dependencia tecnológica.
- No suponer.
- Cada caso de uso debe representar una funcionalidad específica del sistema.
- Definir la interoperabilidad con tecnologías externas mediante interfaces claras cumpliendo las definiciones de las reglas de negocio.
- Mantener una mentalidad abierta durante todo el proceso.

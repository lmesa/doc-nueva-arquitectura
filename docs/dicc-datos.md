# Diccionario de datos

el nombramiento de los datos debe ser:

- Descriptivo: que evite la ambigüedad (`storeName` en vez de `store`)
- Conciso: En caso de nombramientos largos buscar su composición mas corta sin que pierda el sentido (ej: `authTrxId` en vez de `authoritationTransactionId`)
- Sin abreviaturas: Excepto abreviaturas propias del contexto del negocio (ej: `nut`) o muy conocidas en el ámbito técnico (ej: `auth`) usadas como prefijos o sufijos (ej: `type1x1` o `authOrReverse`)
- Con un significado único: por ejemplo el valor de un PLU en el contexto de venta es `itemPrice` pero en el contexto de no venta es `amount`
- En formato `camelCase` o `SCREAMING_SNAKE_CASE`

Esto con el fin de que en su conjunto sean consistentes con el lenguaje técnico y del negocio, mejorando la legibilidad y comprensión del código.

Se deben evitar:

- Nombres genéricos: `data`, `temp`, o `info`
- Mezcla de idiomas: `newVlr`

## Del negocio

|Dato|Descripción|
|-|-|
|amount|valor - precio - monto|
|cashierId|Número de identificación del cajero|
|date|Dato de fecha|
|plu|Identificador del Item|
|storeId|Número de la tienda|
|terminalId|Número de la terminal|
|time|Dato de hora|
|transactionId|Número de la transacción|

## Transversales

|Dato|Descripción|
|-|-|
|messageResponse|Mensaje de respuesta|
|statusResponse|Código de respuesta|

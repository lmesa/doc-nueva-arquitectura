# Comparativa de Servidores de Aplicaciones

## Jetty

### Descripción

Jetty es un servidor web y de aplicaciones Java desarrollado por Eclipse Foundation. Es conocido por su ligereza y flexibilidad. Actualmente se encuentra en la versión 12 lanzada en el año 2023

### Fortalezas

- ___Ligero y rápido:___ Jetty es muy eficiente y tiene un bajo consumo de recursos.
- ___Flexible:___ Fácil de embedir en aplicaciones Java.
- ___Actualizaciones frecuentes:___ Se actualiza regularmente para mantener la seguridad y el rendimiento.
- ___Soporte para WebSockets y HTTP/2:___ Integración nativa y eficiente.

### Debilidades

- ___Menor popularidad:___ No es tan ampliamente adoptado como Tomcat o WildFly.
- ___Funcionalidad limitada fuera del ámbito web:___ No está diseñado para manejar aplicaciones empresariales complejas.

### Soporte y Comunidad

- ___Comunidad activa:___ Aunque no es tan grande como la de Tomcat, tiene una comunidad activa y en crecimiento.
- ___Documentación:___ Buena, con ejemplos claros y guías detalladas.

### Administración y Logueo

- ___Administración:___ Puede ser más complicado de administrar debido a su flexibilidad y configuración.
- ___Logueo:___ Usa logback o slf4j, que son potentes pero requieren configuración manual.

## WildFly

### Descripción

WildFly, anteriormente conocido como JBoss AS, es un servidor de aplicaciones Java EE desarrollado por Red Hat. Actualmente se encuentra en la versión 32 lanzada en el año 2024

### Fortalezas

- ___Completo:___ Soporte completo para Java EE y Jakarta EE.
- ___Escalabilidad:___ Adecuado para aplicaciones empresariales grandes.
- ___Modularidad:___ Gestión eficiente de recursos gracias a su arquitectura modular.
- ___Herramientas de administración:___ Consola de administración web y CLI avanzadas.

### Debilidades

- ___Consumo de recursos:___ Requiere más memoria y CPU en comparación con Jetty y Tomcat.
- ___Complejidad:___ Configuración y administración más complejas debido a su riqueza en funcionalidades.

### Soporte y Comunidad

- ___Comunidad grande y activa:___ Amplio soporte y muchas contribuciones de la comunidad.
- ___Soporte empresarial:___ Red Hat ofrece soporte profesional y actualizaciones.

### Administración y Logueo

- ___Administración:___ Consola de administración web intuitiva y herramientas CLI.
- ___Logueo:___ Sistema de logueo robusto, con integración nativa de JBoss Logging.

## Tomcat

### Descripción

Tomcat es un contenedor de servlets y servidor web de código abierto desarrollado por la Apache Software Foundation. Actualmente se encuentra en la versión 10 lanzada en el año 2021

### Fortalezas

- ___Popularidad:___ Amplia adopción y uso en la industria.
- ___Rendimiento:___ Buen rendimiento para aplicaciones web.
- ___Simplicidad:___ Fácil de configurar y administrar para aplicaciones web.

### Debilidades

- ___Funcionalidad limitada:___ No es un servidor de aplicaciones Java EE completo.
- ___Escalabilidad limitada:___ No es ideal para aplicaciones empresariales muy grandes.

### Soporte y Comunidad

- ___Comunidad grande y activa:___ Gran cantidad de recursos y soporte en línea.
- ___Documentación:___ Extensa y detallada.

### Administración y Logueo

- ___Administración:___ Administración básica pero efectiva a través de scripts y la interfaz de administración web.
- ___Logueo:___ Usa java.util.logging por defecto, con soporte para otros sistemas de logueo.

## Pruebas y Rendimiento

Se realizan las mediciones y pruebas sobre una terminal controladora con `6000Mb` de memoria física y `5865Mb` asignados a linux, conviviendo con el entorno normal de ejecución (Motor, WebPOS y TomEE):

<figure markdown="span">
    ![imagen](assets/servers/mem-inicial.png)
    <figcaption>Medicion sin ServApps arriba</figcaption>
</figure>

Se configuran dos tipo de pruebas, una enfocada a la CPU y otra a la RAM con el fin de observar la estabilidad del servidor y del entorno en sí, ante posibles escenarios de estres. Se toman las siguiente evidencias antes y despúes de la pruebas:

### Jetty

<figure markdown="span">
    ![imagen](assets/servers/jetty.png)
    <figcaption>Instancia servidor sin módulos</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/jetty-2.png)
    <figcaption>Instancia servidor con módulos</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/jetty-3.png)
    <figcaption>Durante prueba CPU</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/jetty-4.png)
    <figcaption>Despues de prueba CPU</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/jetty-7.png)
    <figcaption>Durante de prueba RAM</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/jetty-6.png)
    <figcaption>Despues de prueba RAM</figcaption>
</figure>

### WildFly

<figure markdown="span">
    ![imagen](assets/servers/wildfly.png)
    <figcaption>Instancia servidor sin módulos</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/wildfly-2.png)
    <figcaption>Instancia servidor con módulos</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/wildfly-3.png)
    <figcaption>Durante prueba CPU</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/wildfly-4.png)
    <figcaption>Despues de prueba CPU</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/wildfly-6.png)
    <figcaption>Durante de prueba RAM</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/wildfly-7.png)
    <figcaption>Despues de prueba RAM</figcaption>
</figure>

### Tomcat

<figure markdown="span">
    ![imagen](assets/servers/tomcat.png)
    <figcaption>Instancia servidor sin módulos</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/tomcat-2.png)
    <figcaption>Instancia servidor con módulos</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/tomcat-3.png)
    <figcaption>Durante prueba CPU</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/tomcat-4.png)
    <figcaption>Despues de prueba CPU</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/tomcat-6.png)
    <figcaption>Durante de prueba RAM</figcaption>
</figure>

<figure markdown="span">
    ![imagen](assets/servers/tomcat-7.png)
    <figcaption>Despues de prueba RAM</figcaption>
</figure>

## Tablas comparativas

### Servicios Abajo

|| <span class="pill-green">JETTY</span> | <span class="pill-red">WILDFLY</span>  | <span class="pill-yellow">TOMCAT</span> |
||:-:|:-:|:-:|
| %CPU    | 0.7   | 0.7     | 0.3   |
| %RAM    | 1.5   | 5.7     | 1.7    |
| QRAM-Mb | 88.0  | 334.3   | 99.7   |

### Servicios Arriba

|| <span class="pill-yellow">JETTY</span> | <span class="pill-red">WILDFLY</span>  | <span class="pill-green">TOMCAT</span> |
||:-:|:-:|:-:|
| %CPU    | 0.0   | 0.0     | 0.3   |
| %RAM    | 9.3   | 11.9     | 8.6    |
| QRAM-Mb | 545.4  | 697.9   | 504.4   |

### Test CPU

La prueba consistío en ejecutar 40 peticiones desde dos clientes a un algoritmo de ordenación para un array de tamaño de 10000 números.

Lectura durante el test:

|| <span class="pill-yellow">JETTY</span> | <span class="pill-red">WILDFLY</span>  | <span class="pill-green">TOMCAT</span> |
||:-:|:-:|:-:|
| %CPU    | 106.0  | 129.2     | 102.0   |
| %RAM    | 9.7   | 12.2     | 9.7|
| QRAM-Mb | 568.9  | 715.5   | 568.9 |

Lectura terminado el test:

|| <span class="pill-green">JETTY</span> | <span class="pill-red">WILDFLY</span>  | <span class="pill-green">TOMCAT</span> |
||:-:|:-:|:-:|
| %CPU    | 0.0  | 0.3     | 0.0   |
| %RAM    | 9.8   | 14.3     | 9.8|
| QRAM-Mb | 574.8  | 838.7   | 574.8 |

### Test Memoria

La prueba consistío en ejecutar mediante una petición un algoritmo que produzca una saturación de la memoria.

Lectura durante el test:

|| <span class="pill-yellow">JETTY</span> | <span class="pill-red">WILDFLY</span>  | <span class="pill-green">TOMCAT</span> |
||:-:|:-:|:-:|
| %CPU    | 156.1  | 141.2     | 130.6   |
| %RAM    | 20.1   | 22.3    | 19.5|
| QRAM-Mb | 1178.9  | 1307.9   | 1143.7 |

Lectura terminado el test:

|| <span class="pill-green">JETTY</span> | <span class="pill-yellow">WILDFLY</span>  | <span class="pill-red">TOMCAT</span> |
||:-:|:-:|:-:|
| %CPU    | 0.0  | 0.0    | 0.3   |
| %RAM    | 17.5   | 18.8     | 19.5|
| QRAM-Mb | 1026.0  | 1102.6   | 1143.7 |

Esta tabla resume la percepción obtenida en la instalación, manipulacion y pruebas.

| Característica| Jetty| WildFly| Tomcat|
|-|:-:|:-:|:-:|
|___Version testeada___| 9.4.54 (2016)|26.1.3 (2023)|9.0.90 (2017)|
| ___Ligereza___          | Alta                       | Media                      | Alta                      |
| ___Complejidad[^1]___       | Baja                       | Alta                       | Baja                      |
| ___Funcionalidades[^2]___   | Limitadas                  | Completas                  | Limitadas                 |
| ___Administración___    | Flexible pero compleja     | Avanzada y completa        | Básica pero efectiva      |
| ___Comunidad___         | Activa pero pequeña        | Grande y activa            | Grande y activa           |
| ___Soporta Java 8___         | Si        | Si            | Si           |
| ___Soporta SpringBoot 2.7.12___         | Si        | Si            | Si           |

[^1]: Configuración inicial, despliegue de aplicaciones, administración y mantenimiento.
[^2]: Soporte para especificaciones empresariales, servicios integrados, extensibilidad y plugins, herramientas de administración, soporte para frameworks y librerías.

## Conclusión General

Jetty exhibe consistentemente un bajo consumo de CPU y RAM tanto en condiciones normales como bajo carga, aunque muestra un aumento significativo en el uso de memoria durante pruebas específicas de este recurso.

WildFly presenta un uso moderado a alto de recursos, especialmente en términos de RAM y memoria, mostrando robustez bajo cargas intensivas de CPU y memoria en comparación con Jetty y Tomcat.

Tomcat se distingue por su eficiencia en el uso de recursos, manteniendo niveles bajos de consumo de CPU y RAM en la mayoría de las pruebas, con un rendimiento estable incluso bajo cargas moderadas a intensivas.

Cada servidor tiene sus fortalezas: Jetty es ideal para aplicaciones livianas y embebidas que valoran la simplicidad y el rendimiento, aunque puede ser menos adecuado para personalizaciones complejas. WildFly se destaca en entornos empresariales complejos que requieren todas las funcionalidades de Java EE, ofreciendo una administración avanzada y numerosas opciones de configuración. Tomcat, conocido por su simplicidad y buen rendimiento, es óptimo para aplicaciones web que no necesitan todas las capacidades de Java EE, especialmente con la familiaridad previa con TomEE facilitando su configuración.

Es importante tener en cuenta que las versiones actuales de estos servidores están próximas a la deprecación y versiones futuras podrían no ser compatibles con Java Development Kit 8 (TDK 8).

La elección entre Jetty, WildFly y Tomcat dependerá principalmente de las necesidades específicas de recursos y rendimiento de las aplicaciones, sin profundizar en los detalles de implementación de cada servidor.

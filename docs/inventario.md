# Inventarios

## Componentes

|NOMBRE|SERVICIO|ARCHIVO FW.DAT|JAR|DEPENDENCIAS|
|-|-|-|-|-|
|[Convenios](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-convenios-termFw-Jconvenios)|Módulo que permite controlar pagos, consultas y demás operaciones de la tecla de convenios.|`FWCONVE.DAT`|`JConvenios.jar`<br>`vr 1.4`|`VentasPagos`<br>`BbRulesValidator`|
|[Garantía<br>Extendida](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-garantiaEntrega-termFw-CidomGarantiaExtendida)|Modulo para el control de los servicios de CIDOM (control de domicilios)y garantextendida en POS|`FWGECID.DAT`|`CidomGarantiaExtendida.jar`<br>`vr 1.4`|`VentasPagos`<br>`OS4690TerminalUtilities`|
|[Venta<br>Contenidos](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-contenido-termFw-VentaContenidos)|Módulo que controla las operaciones del servicio de venta de contenidos.|`FWVDCONT.DAT`|`VentaContenidos.jar`<br>`vr 1.4`|`VentasPagos`|
|[Facturador](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-ventaAutomaticas-termFrameworkPromo-Faccedicen)|Módulo de facturacturación POS para transacciones de Picking.|`FWMAIN.DAT`|`FacCedicen.jar`<br>`vr 1.8`|`ApiComunicaciones`<br>`Modelo`<br>`Reportes`<br>`slf4j-log4j12`<br>`os4690`<br>`FwUtil`<br>`AEFExtension`<br>`SoaEngine`<br>`DataStore`<br>`SIUser`<br>`libase`|
|Orquestador|Productos asociados al PINPAD (revisar)|-|`FwKeyOperations.jar`<br>`vr 1.8`|`VentasPagos`|
|[Ventas<br>Pagos](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-reglas-termFw-FrameworkContenidosPagos)|Posible orquestador ya que tiene mas lógica|-|`FrameworkContenidosPagos.jar`<br>`vr 1.8`|`ApiPersistencia`<br>`ApplicationLayer`<br>`Clifre`<br>`DomainLayer`<br>`FwConf`<br>`Modelo`<br>`os4690` `Presentacion`<br>`Reportes`<br>`smartmod`<br>|
|[Api<br>Comunicaciones](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-comunicacion-term-ApiComunicaciones)|Modulo encargado de realizar la comunicacion hacia el controlador.|-|`ApiComunicaciones.jar`<br>`vr 1.8`|`ApiPinpad`<br>`comm4690`<br>`FwUtil`<br>`jackson-core`<br>`jersey-container-servlet`<br>`jersey-media-json-jackson`<br>`Modelo`<br>`OS4690TerminalUtilities`<br>`rxtxComm`<br>`ServiceConf`<br>`SoaEngine`<br>`TransactionalMessageService`|

<figure markdown="span">
    ![ark](assets/inventario/001.svg)
    <!-- <figcaption>Arquitectura Dominio</figcaption> -->
</figure>

## Otros componentes<br><sub><sup>(marcados ya se  encuentran deprecados)

- ___Carpeta \bp___

- [ ] `FwOperations.jar`
- [x] `GirosRemesas.jar`
- [x] `PagosEfectivoExitoCom.jar`
- [x] `PuntosExito.jar`
- [ ] `SecurityKey.jar`
- [ ] `TarjetasPropias.jar` <sub><sup>(en proceso)
- [ ] `Telefonia.jar` <sub><sup>(en proceso)

- ___Carpeta \platform___

- [ ] `PinpadManaKey.jar` <sub><sup>(en proceso)
- [x] `SumaDescuentos.jar` <sub><sup>(ya lo hace motor)
- [x] `CargaEventos.jar` <sub><sup>(ya lo hace motor)
- [x] `Clifre.jar` <sub><sup>(sigue ligado a convenios)
- [x] `FiadoExito.jar`

- ___Carpeta \srv___

- [x] `Application.jar`
- [ ] `DataStore.jar`
- [ ] `DigitalReceipt.jar`
- [ ] `Director.jar`
- [x] `Domain.jar`
- [x] `DomainModel.jar`
- [ ] `Interface.jar`
- [ ] `J8583GrupoExito.jar`
- [ ] `KeyedReader.jar`
- [ ] `Maintenance.jar`
- [ ] `MaintenanceModel.jar`
- [ ] `MessagingHandler.jar`
- [ ] `MessagingHandlerISO.jar`
- [ ] `SecurityService.jar`
- [x] `ServiceConf.jar`
- [ ] `SoaEngine.jar`
- [ ] `TerminalPersistence.jar`
- [ ] `TransactionalMessageService.jar`
- [ ] `ValidationResponseRevertionStatus.jar`

- ___Carpeta \libs___

- [ ] `AbsoluteLayout-RELEASE160.jar`
- [ ] `AEFExtension.jar`
- [ ] `Antlr-2.7.6.jar`
- [ ] `aopalliance-repackaged-2.5.0-b32.jar`
- [ ] `apiguardian-api-1.1.2.jar`
- [ ] `Backport-util-concurrent.jar`
- [ ] `batik-anim-1.16.jar`
- [ ] `batik-awt-util-1.16.jar`
- [ ] `batik-bridge-1.16.jar`
- [ ] `batik-codec-1.9.jar`
- [ ] `batik-constants-1.16.jar`
- [ ] `batik-css-1.16.jar`
- [ ] `batik-dom-1.16.jar`
- [ ] `batik-ext-1.16.jar`
- [ ] `batik-gvt-1.16.jar`
- [ ] `batik-i18n-1.16.jar`
- [ ] `batik-parser-1.16.jar`
- [ ] `batik-script-1.16.jar`
- [ ] `batik-shared-resources-1.16.jar`
- [ ] `batik-svg-dom-1.16.jar`
- [ ] `batik-svggen-1.16.jar`
- [ ] `batik-transcoder-1.16.jar`
- [ ] `batik-util-1.16.jar`
- [ ] `batik-xml-1.16.jar`
- [x] `Bbrv.jar`
- [ ] `BeanShell.jar`
- [ ] `Biometria.jar`
- [ ] `comm-2.0-2.0.0.jar`
- [ ] `comm4690-1.0.0.jar`
- [ ] `Commons-beanutils-1.7.0.jar`
- [ ] `Commons-collections-3.1.jar`
- [ ] `Commons-collections-3.2.jar`
- [ ] `commons-io-2.11.0.jar`
- [ ] `Commons-lang-2.4.jar`
- [ ] `Commons-logging-1.1.1.jar`
- [ ] `Commons-logging.jar`
- [ ] `Commons-net.jar`
- [ ] `core-3.4.0.jar`
- [ ] `DisplayQRCode.jar`
- [ ] `Dom4j-1.6.1.jar`
- [ ] `Ezmorph-1.0.6.jar`
- [ ] `Freemarker-2.3.23.jar`
- [x] `FwUtil.jar`
- [ ] `GenerateQRCodeStandAlone.jar`
- [ ] `Gson-2.8.2.jar`
- [ ] `hamcrest-core-1.3.jar`
- [ ] `Hibernate-core-3.3.2.GA.jar`
- [ ] `Hk2-Api-2.5.0-b32.jar`
- [ ] `Hk2-Locator-2.5.0-b32.jar`
- [ ] `Hk2-Utils-2.5.0-b32.jar`
- [ ] `Hsqldb-1.7.3.3.jar`
- [ ] `Jackson-Annotations-2.8.4.jar`
- [ ] `Jackson-Annotations-2.9.0.jar`
- [ ] `Jackson-Core-2.9.4.jar`
- [ ] `Jackson-Core-2.9.6.jar`
- [ ] `Jackson-Databind-2.9.6.jar`
- [ ] `Jackson-Jaxrs-Base-2.8.4.jar`
- [ ] `Jackson-Jaxrs-Json-Provider-2.8.4.jar`
- [ ] `Jackson-Module-Jaxb-Annotations-2.8.4.jar`
- [ ] `jai-imageio-core-1.4.0.jar`
- [ ] `jattach-4.1.1010.jar`
- [ ] `javase-3.4.0.jar`
- [ ] `Javassist-3.4.GA.jar`
- [ ] `Javax.annotation-Api-1.2.jar`
- [ ] `Javax.inject-2.5.0-b32.jar`
- [ ] `Javax.ws.rs-Api-2.0.1.jar`
- [ ] `Jcl-over-slf4j-1.6.6.jar`
- [ ] `jcommander-1.72.jar`
- [ ] `Jdom.jar`
- [ ] `Jersey-Client-2.25.1.jar`
- [ ] `Jersey-Common-2.25.1.jar`
- [ ] `Jersey-Container-Servlet-2.25.1.jar`
- [ ] `Jersey-Container-Servlet-Core-2.25.1.jar`
- [ ] `Jersey-Entity-Filtering-2.25.1.jar`
- [ ] `Jersey-Guava-2.25.1.jar`
- [ ] `Jersey-Media-Jaxb-2.25.1.jar`
- [ ] `Jersey-Media-Json-Jackson-2.25.1.jar`
- [ ] `Jersey-Server-2.25.1.jar`
- [ ] `Json-lib-2.2.3.jar`
- [ ] `Jta-1.1.jar`
- [x] `Libase-1.3.jar`
- [ ] `Log4j-1.2.17.jar`
- [x] `Modelo.jar`
- [ ] `Os4690TerminalUtilities.jar`
- [ ] `PackFunction.jar`
- [x] `Persist.jar`
- [x] `PinpadService.jar`
- [x] `Presentacion.jar`
- [x] `PrintExtension.jar`
- [ ] `Reactive-Streams-1.0.2.jar`
- [ ] `Reactor-Core-3.2.3.RELEASE.jar`
- [x] `Reportes.jar`
- [ ] `Serial.jar`
- [ ] `Slf4j-api-1.6.6.jar`
- [ ] `Slf4j-log4j12-1.6.6.jar`
- [ ] `SmartMod.jar`
- [ ] `Spring.jar`
- [ ] `Xerces.jar`
- [ ] `Xml-apis-1.0.b2.jar`
- [ ] `xml-apis-ext-1.3.04.jar`
- [ ] `xmlgraphics-commons-2.7.jar`
- [ ] `xpp3_min-1.1.3.4.O.jar`
- [ ] `Xstream-1.1.2.jar`

## Comentarios

- Que funcionalidades están en Framework actualmente y se encuentran migrados o proximos a migrar

### Servicios migrados

- Giros
- Pines
- Puntos Exito
- Suma descuentos <sub><sup>(migrado en Motor de promociones)
- Carga eventos <sub><sup>(migrado en Motor de promociones)
- Todos los `HCH` relacionados del lado del `PAF`

### Próximos a Migrár

- Telefonia.jar
- Pinpad
- Fiado Exito
- Todos los `HCH` relacionados del lado del `PAF`

- Consideración de nuevos servicios o reutilización de servicios del lado del servidor de aplicaciones. Evaluar descontinuacion del PAF

- Pendiente (Alinear con arquitectura de lo ya migrado - objetivo es reutilización de componentes)

- ¿Que actores salen?<br><sub><sup>(falta por definir la correspondencia con cada uno de los servicios.)

<figure markdown="span">
    ![ark](assets/inventario/002.svg)
    <!-- <figcaption>Arquitectura Dominio</figcaption> -->
</figure>

### ¿Hay reducción de código del lado de BASIC, llamados, retiro de módulos?

- Pendiente (Se necesita conocer los flujos para saber si hay interacción con componentes Basic)

### Estrategia de Logs

- Definir si se deben llevar log independientes por servicio o uno solo como está actualmente.

### En la medida del análisis dimensionar que otros recursos vamos a necesitar

- Realización de pruebas para observar los flujos y otros componentes que puedan interactuar.
- Recursos de `autorizadores`
- Set de pruebas

### Evaluacion de tecnologias alternativas

- Pendiente (Se consideraría segun las pruebas de flujo actual)

### Lista tentativa de repositorios a dar de baja

- [BbRulesValidator](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-reglasTerminal-term-BbRulesValidator)
- [CargaEventos](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-eventos-term-CargaEventos)
- [clifre](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-clifre-termFw-clifre)
- [fastpay](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-autoriza-contPAFActor-fastpay)
- [FiadoExito](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-tarjetaFiado-termFw-FiadoExito)
- [FwUtil](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-libreriaUtilidades-termDominioPromo-FwUtil)
- [GirosRemesas](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-giroRemesa-termFw-GirosRemesas)
- [hchcidom](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-entregaCasa-contPAFHCH-hchcidom)
- [hchconte](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-ventaContenido-contPAFHCH-hchconte)
- [hchfac](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-facturador-contPAFHCH-hchfac)
- [hchgiros](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-giro-contPAFHCH-hchgiros)
- [hchpc](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-puntosCol-contPAFHCH-hchpc)
- [hchpines](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-pinExito-contPAFHCH-hchpines)
- [hchtel](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-movilExito-contPAFHCH-hchtel)
- [hchtr](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-tarjetaRegalo-contPAFHCH-hchtr)
- [Modelo](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-modeloDB-termFw-Modelo)
- [PagosEfectivoExitoCom](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-pinesVirtual-term-Fw-PagosEfectivoExitoCom)
- [PinpadManaKey](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-actualizacionPinPad-term-PinpadManaKey)
- [PinpadService](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-driverPinPad-term-PinpadService)
- [Presentacion](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-presentacion-termFw-Presentacion)
- [properties](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-propiedades-termFw-properties)
- [PuntosExito](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-redencion-termFw-PuntosExito)
- [SumaDescuentos](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-descuentos-term-SumaDescuentos)
- [TarjetasPropias](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-tarjetas-termFw-TarjetasPropias)
- [Telefonia](https://dev.azure.com/grupo-exito/GCIT-Agile/_git/pos-telefoniaExito-termFw-Telefonia)

## Pros y Contras

### Pros

- Mantenibilidad.
- Refactorización de cara a mejorar el rendimiento.
- Posibilidad de implemetar Pruebas Unitarias.
- Depuración de código y componentes obsoletos o huerfanos.
- Depuracion en la precarga

### Contras

- Riesgo de inestabilidad en el servicio.
- Depuracion de reportes
- Configuracion de `ATN`
- convivencia de ambos (switch de funcionalidad)

### Riesgos

- inclusión de recursos no disponibles
- Costos no contemplados en esta etapa

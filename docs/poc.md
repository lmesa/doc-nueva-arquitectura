# Pruebas de Concepto

## Estrategia de logeo

### Actuator

Es un módulo de Spring Boot que proporciona funcionalidades para monitorear y administrar una aplicación. Actuator expone varios puntos finales (endpoints) que permiten acceder a información operativa de la aplicación, tales como métricas, detalles del entorno, información sobre beans, detalles de configuración, y más. Estos endpoints pueden ser muy útiles para tareas de administración y monitoreo.

Algunos de los endpoints más comunes que proporciona Actuator incluyen:

- `/actuator/health`: Proporciona información sobre el estado de salud de la aplicación.
- `/actuator/info`: Muestra información general de la aplicación, como la versión y otros datos personalizados.
- `/actuator/metrics`: Ofrece métricas detalladas de la aplicación, como uso de memoria, número de solicitudes HTTP, tiempos de respuesta, etc.
- `/actuator/env`: Proporciona detalles sobre las propiedades del entorno y sus valores.
- `/actuator/beans`: Muestra una lista de todos los beans de Spring en la aplicación.
Para habilitar Actuator en una aplicación Spring Boot, simplemente añade la dependencia spring-boot-starter-actuator en tu archivo pom.xml:

```xml title="POM.xml"
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```

```properties title="application.properties"
management.endpoints.web.exposure.include=*
```

### Programación orientada a aspectos (AOP)

La programación orientada a aspectos (AOP) es conveniente de implementar en varios escenarios, especialmente cuando:

Aspectos transversales: Hay funcionalidades que se repiten en diferentes partes de una aplicación y no están directamente relacionadas con la lógica principal de negocio, como el logging, la seguridad, la gestión de transacciones, etc.

Separación de preocupaciones: Permite separar el código que implementa funcionalidades específicas de la aplicación (conocido como código principal o código base) de las funcionalidades que afectan a varios módulos o capas (conocido como código de aspecto).

Mejora de la modularidad: Facilita la reutilización y la gestión de cambios al desacoplar aspectos específicos de la aplicación de su lógica central.

Mejora de la mantenibilidad: Simplifica la gestión de aspectos que atraviesan múltiples componentes o capas de una aplicación, reduciendo la duplicación de código y mejorando la legibilidad y mantenibilidad del código principal.

En resumen, AOP es útil cuando se desea modularizar y gestionar de manera eficiente aspectos que se aplican transversalmente a través de una aplicación, sin mezclarlos con la lógica principal del negocio.

### Beneficios de combinar Actuator con AOP

- ___Monitoreo Dinámico___: Puedes ajustar dinámicamente los niveles de logging en función del comportamiento en tiempo de ejecución.
- ___Separación de Preocupaciones___: AOP permite mantener el código de logging separado del código de negocio, manteniendo el código más limpio y fácil de mantener.
- ___Mejor Administración___: Actuator proporciona puntos finales que permiten la administración y monitoreo de la aplicación sin necesidad de cambios en el código fuente.

Combinar Actuator con AOP puede proporcionar una solución robusta para la administración y monitoreo avanzado de aplicaciones Spring Boot.

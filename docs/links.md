# Links de Interés

- [Arquitectura Orientada a Servicios SOA](<https://aws.amazon.com/es/what-is/service-oriented-architecture/ >)
- [Arquitectura Orientada a Microservicios](<https://herbertograca.com/2017/01/26/microservices-architecture/ >)
- [Arquitectura Hexagonal](<https://herbertograca.com/2017/09/14/ports-adapters-architecture/ >)
- [DDD, Hexagonal, Onion, Clean, CQRS, … How I put it all together](<https://herbertograca.com/2017/11/16/explicit-architecture-01-ddd-hexagonal-onion-clean-cqrs-how-i-put-it-all-together/?utm_content=buffer01772&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer>)
- [Trello del proyecto](<https://trello.com/b/SJpRw347/tareas>)
- [Azure del proyecto](<https://grupo-exito.visualstudio.com/GCIT-Agile/_backlogs/backlog/DO%20-%20SAV%20Tecnico/Release?workitem=519300>)
- [Wiki del proyecto](<https://dev.azure.com/grupo-exito/GCIT-Agile/_wiki/wikis/GCIT-Agile.wiki/14617/Arquitectura-por-servicios>)

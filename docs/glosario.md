# Glosario

___Adaptadores___: Son las implementaciones concretas de los puertos. Contienen la lógica específica necesaria para conectar el núcleo de la aplicación con tecnologías y servicios externos. Los adaptadores traducen las llamadas entre el núcleo de la aplicación y los sistemas externos. (Adaptadores de entrada y salida)

___Contexto de la TRX___: Son todos los datos relacionados en el entorno de una compra o procedimeinto de no venta.

___Contexto de Ejecucion de JAVA___: Es la conjunto de información que fluye de manera unidireccional desde Java hacia BASIC como respuesta al resultado de una operación.

___Modelo del Dominio___: Describe las reglas, cálculos y procesos que son fundamentales para la operación del negocio. Acá está la logica del comportamiento esperado del componente y debe ser aislado de componentes externos.

___Puertos___: Son interfaces que definen cómo el núcleo de la aplicación se comunica con el mundo exterior y viceversa. Los puertos no contienen lógica de negocio ni detalles de implementación; simplemente definen contratos que deben cumplirse para interactuar con el núcleo de la aplicación. (Puertos primarios y secundarios)

___Datos de Transaccion___: Son los datos transversales que se encuentran en una transaccion y hacen parte de la petición del lado del sistema POS.

___Datos complementarios___: Son los datos particulares de cada una de las operaciones y son proporcionados en el transcurso de la operación.
